<?php
/**
 * @package WP Scraper Pro
 * @version 5.1  */
/*
Plugin Name: WP Scraper Pro
Plugin URI:  http://www.wpscraper.com
Description: This plugin allows you to copy content from websites directly to your WordPress posts.
Version: 5.1
Author: Allyson Rico, Robert Macchi
*/

define( 'WPSP_DIR', untrailingslashit( dirname( __FILE__ ) ) );

global $wp_scraper_pro_db_version;
$wp_scraper_pro_db_version = '1.5';


if ( is_admin() ) {
    add_action( 'admin_menu', 'WpScraperPro::wp_scraper_pro_menu');
	add_action( 'admin_menu', 'wpsp_edit_admin_menus' );
}

add_action( 'admin_enqueue_scripts', 'wpsp_admin_enqueue_scripts' );

function wpsp_edit_admin_menus() {
	global $submenu;

	if ( current_user_can( 'activate_plugins' ) ) {
		$submenu['wp-scraper-pro'][0][0] = 'Single Scrape';
	}
}


class WpScraperPro {

private static $templateVariables;

    // post url
    public static $url;
	
    // post html
    public static $html;
	
    // post images
    public static $images;
	
	
/**
 * Register WP Scraper Menu
 */
 
 public static function wp_scraper_pro_menu($action='') {
        // Main menu block
        $action = (isset($_GET['action']) && (!empty($_GET['action'])))?$_GET['action']:'add';
		
		add_menu_page( 'WP Scraper Single Selection',
            'WP Scraper Pro',
            'activate_plugins', 'wp-scraper-pro',
            'WpScraperPro::wp_scraper_pro_page', 'dashicons-layout', '11.952144500145214' );
		
		$wp_scraper_pro_subpage = add_submenu_page(
        'wp-scraper-pro',
        'WP Scraper Multiple Selection',
        'Multiple Scrape',
        'activate_plugins',
		'wp-scraper-pro-url-menu',
        'wp_scraper_pro_url_page'); 
		
		$wp_scraper_pro_subpage1 = add_submenu_page(
        'null',
        'WP Scraper Url',
        'Multiple Scrape2',
        'activate_plugins',
		'wp-scraper-pro-add-menu',
        'WpScraperPro::wp_scraper_pro_page'); 
		
		$wp_scraper_pro_subpage3 = add_submenu_page(
        'null',
        'WP Scraper Results',
        'Multiple Scrape3',
        'activate_plugins',
		'wp-scraper-pro-results-menu',
        'wp_scraper_pro_results_page');  
		
		$wp_scraper_pro_subpage2 = add_submenu_page(
        'wp-scraper-pro',
        'WP Scraper Help',
        'Help',
        'activate_plugins',
		'wp-scraper-pro-help-menu',
        'wp_scraper_pro_help_page'); 
		
		$path = 'wp-scraper-pro'.($action?'&action='.$action:'');
		
		$the_page = isset($_GET['page'])?$_GET['page']:null;
		
		if($the_page != 'wp-scraper-pro') return;

		$function = 'wp_scraper_pro_'.$action.'_content';
		WpScraperPro::$templateVariables = WpScraperPro::$function();
        

    }
	
	public static function wp_scraper_pro_page($vars = array(), $page='wp-scraper-pro', $template=null) {
		add_thickbox();
		require_once('includes/meta-boxes.php');
		//add_meta_box( 'submitdiv', __( 'Publish' ), 'post_submit_meta_box', 'toplevel_page_wp-scraper-pro', 'side', 'core' );
		add_meta_box( 'categorydiv', __( 'Categories' ), 'wpsp_post_categories_meta_box', 'toplevel_page_wp-scraper-pro', 'side', 'core' );
		add_meta_box( 'tagsdiv-post_tag', __( 'Tags' ), 'wpsp_post_tags_meta_box', 'toplevel_page_wp-scraper-pro', 'side', 'core' );
		add_meta_box( 'postimagediv', __( 'Featured Image' ), 'wpsp_post_thumbnail_meta_box', 'toplevel_page_wp-scraper-pro', 'side', 'core' );
        if (!$template) {
            $template = (isset($_GET['action']) && !empty($_GET['action']))?$_GET['action']:'add';
        }
		if (isset($_GET['page'])) {
			$this_page = $_GET['page'];
		}
		
		if(isset($_POST['url_list']) || $this_page == 'wp-scraper-pro-add-menu') {
			if(!$_POST['url_list']) { 
				echo '<div id="message" class="error notice">
					<p>
					Something went wrong. Your list of urls wasn\'t sent. Please go back and try again. Thank you.
					</p>
				</div>';}
			
			$url_list = $_POST['url_list'];
			$url_list = trim($url_list);
			$url_list = explode(',', $url_list);
			$new_list = implode(',', $url_list);
			if (!$vars || !count($vars)) {
            $vars = WpScraperPro::$templateVariables;
			//$temp_dump = print_r(WpScraperPro::$templateVariables, true);
			}
			if (isset($vars)) {
				extract($vars);
			}
			if ($template == 'add') {
				$post_type_options = '';
				$args = array(
				   'public'   => true,
				);
				foreach ( get_post_types( $args, 'names' ) as $post_type ) {
				   if ($post_type == 'attachment') continue;
				   if ($post_type == 'post') {
					   $selected = 'selected="selected"';
				   } else $selected = '';
				   $post_type_options .= '<option value="'.$post_type.'"'.$selected.'>' . ucfirst($post_type) . '</p>';
				}
				echo '<div class="wrap wpsp-form">';
			echo '<h2>Add Multiple Scraped Posts</h2>
				<form method="post" action="'.admin_url().'admin.php?page=wp-scraper-pro-results-menu" id="wpsp-add-multi-post-form"  enctype="multipart/form-data">
					<input type="hidden" value="'.$new_list.'" id="wpsp-url-list" name="wpsp-url-list" />
					<input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'auto').'" id="wpsp-content-auto-url" />
					<input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'extract').'" id="wpsp-content-extractor-url" />
					<input type="hidden" value="true" id="wpsp_is_mult" />
					<input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'downloader').'" id="wpsp-downloader-url" />';
					wp_nonce_field( 'wpsp-save-wpscraper'); 
					echo '<div id="wpsp-add-source-form-container" class="metabox-holder">
						<div id="wpsp-extractor-box" style="display:none">
							<div class="field wpsp-field-container">
								<input id="wpsp-url" class="regular-text ltr" name="url" value="'.$url_list[0].'" />
							</div>
						</div>
						<div id="add_wpsp_post_container">
						<div id="postbox-container-1" class="postbox-container">
						<div id="titlediv" class="wpsp-field-container">
							<input class="wpsp-selector" type="text" name="title_selector" value="" id="title_selector" />
							<input type="text" name="title_prefix" size="80" value="" id="title_prefix" spellcheck="true" placeholder="Prefix" /><input type="text" name="title" size="80" value="" id="title" spellcheck="true" placeholder="Enter post title" /><input type="text" name="title_suffix" size="80" value="" id="title_suffix" spellcheck="true" placeholder="Suffix" />
							<a id="choose_title_content" title="Click to select content you want to use for the title. Then click the button below to add it to the title field." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button block-select-btn">Choose Title</a>
						</div>
			
						<div id="wpsp-data" class="wpsp-meta-box-container meta-box-sortables">
							<div class="postbox">
								<h3 class="hndle"><span>Post Content</span></h3>
								
								<div class="inside">
									<div class="field wpsp-field-container">
									<input class="wpsp-selector" type="text" name="body_selector" value="" id="body_selector" /><div id="choose_body"><a id="choose_body_content" title="Click to select content you want to use for the post content. Then click the button below to add it to the post content field." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button block-select-btn">Choose Post Content</a></div>';
									add_action('media_buttons_context', 'wpsp_body_button');
									wp_editor( '', 'wpsp-html', array('media_buttons'=> false) );
										echo '<input type="hidden" id="wpsp-images" name="images" />
							
									</div>
								</div>
							</div>
						</div>
						<div id="extract-options" class="postbox">
							<div class="handlediv" title="Click to toggle">
							<br>
							</div>
							<h3 class="hndle ui-sortable-handle">
							<span>Extract Options</span>
							</h3>
							<div class="inside">
							<label class="extract-opt" for="js" style="display: inline-block"><input id="js" type="checkbox" name="js" value="1">Load JavaScript</label><p class="description">Some content may need javascript enabled to display correctly. Check this box to enable javascript while selecting content.</p><br>
							<label class="extract-opt" for="down" style="display: inline-block"><input id="down" type="checkbox" name="down" value="1">Load Restricted Image Content</label><p class="description">Some images will not load due to cross domain conflicts. Use this feature to load these restricted images. However, it doesn\'t work with all server configurations. Use with caution.</p><br>
							<hr>
							<label class="misc-pub-section" for="wpspinterval" style="display: inline-block; font-weight: bold;">Time Delay Between Scrapes</label><br/>
							<p class="description">This option will scrape one page at a time with this delay between each post. This will help manage your server resources.</p><br>
							<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="0" >None</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="10000" checked>Ten Seconds</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="30000" >Thirty Seconds</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="60000" >One Minute</label>
							</div>
						</div>
						</div>
						<div id="postbox-container-2" class="postbox-container">
						<div id="submitdiv" class="postbox">
							<div class="handlediv" title="Click to toggle">
							<br>
							</div>
							<h3 class="hndle ui-sortable-handle">
							<span>Publish</span>
							</h3>
							<div class="inside">
							<div id="submitpost" class="submitbox">
							<div id="minor-publishing">
							<div id="misc-publishing-actions">
							<div class="misc-pub-section misc-pub-post-type">
							<label for="post_type">Post Type:</label>
							<span id="post-type-display">Post</span>
							<a class="edit-post-type hide-if-no-js" href="#post_type" style="display: inline;">
							<span aria-hidden="true">Edit</span>
							<span class="screen-reader-text">Edit type</span>
							</a>
							<div id="post-type-select" class="hide-if-js" style="display: none;">
							<input id="hidden_post_type" type="hidden" value="post" name="hidden_post_type">
							<select id="post_type" name="post_type">'.$post_type_options.'</select>
							<a class="save-post-type hide-if-no-js button" href="#post_type">OK</a>
							<a class="cancel-post-type hide-if-no-js button-cancel" href="#post_type">Cancel</a>
							</div>
							</div>
							<div class="misc-pub-section misc-pub-post-status">
							<label for="post_status">Status:</label>
							<span id="post-status-display">Published</span>
							<a class="edit-post-status hide-if-no-js" href="#post_status" style="display: inline;">
							<span aria-hidden="true">Edit</span>
							<span class="screen-reader-text">Edit status</span>
							</a>
							<div id="post-status-select" class="hide-if-js" style="display: none;">
							<input id="hidden_post_status" type="hidden" value="publish" name="hidden_post_status">
							<select id="post_status" name="post_status">
							<option value="publish" selected="selected">Published</option>
							<option value="pending">Pending Review</option>
							<option value="draft" selected="selected">Draft</option>
							</select>
							<a class="save-post-status hide-if-no-js button" href="#post_status">OK</a>
							<a class="cancel-post-status hide-if-no-js button-cancel" href="#post_status">Cancel</a>
							</div>
							</div>
							<div class="clear"></div>
							</div>
							</div>
							</div>
							</div>
							<div id="major-publishing-actions">
								<div class="save-wpscraper-form">
									<input id="auto_submit" type="submit" class="button-primary" name="save" value="Create Posts" />
								</div>
							</div>
						</div>
						<div id="submitdiv" class="postbox">
							<div class="handlediv" title="Click to toggle">
							<br>
							</div>
							<h3 class="hndle ui-sortable-handle">
							<span>Post Options</span>
							</h3>
							<div class="inside">
							<label class="misc-pub-section" for="incimages" style="display: inline-block"><input id="incimages" type="checkbox" name="incimages" value="add" checked="checked">Include Images</label><br>
							<label class="misc-pub-section" for="incvideos" style="display: inline-block"><input id="incvideos" type="checkbox" name="incvideos" value="add" checked="checked">Include Videos</label><br>
							<label class="misc-pub-section" for="inctables" style="display: inline-block"><input id="inctables" type="checkbox" name="inctables" value="add" checked="checked">Format Tables</label><br>
							<label class="misc-pub-section" for="remove_links" style="display: inline-block"><input id="remove_links" type="checkbox" name="remove_links" value="remove" >Remove Links</label><br>
							<label class="misc-pub-section" for="add_copy" style="display: inline-block"><input id="add_copy" type="checkbox" name="add_copy" value="add" >Add source link to the content</label><br>
							<label class="misc-pub-section" for="fix" style="display: inline-block"><input id="fix" type="checkbox" name="fix" value="1">Add Prefix and Suffix to all titles</label><br>
							<hr>
							<label class="misc-pub-section" for="htmlelems" style="display: inline-block">HTML to Include:</label><br/>
							<label class="misc-pub-section"><input type="radio" name="htmlelems" value="none" checked >Strip All HTML Elements</label><br><br>
							<label class="misc-pub-section"><input type="radio" name="htmlelems" value="all" >Include All Post HTML Elements</label><br><br>
							<label class="misc-pub-section"><input type="radio" name="htmlelems" value="simple" >Include Basic HTML Elements</label><br><br>
							<label class="misc-pub-section"><input type="radio" name="htmlelems" value="specific" >Include Specific HTML Elements</label><br>
							<textarea id="wpsp-elements" rows="3" cols="27" class="wpsp-elements" name="elements" style="margin: 10px; width:95%;" disabled >br, b, em, strong, mark, i, u, div, h1, h2, h3, h4, h5, h6, li, p, span, ul, ol</textarea><br>
							</div>
						</div>';
						do_meta_boxes('toplevel_page_wp-scraper-pro', 'side', '');
						echo '</div>
					</div>
					</div>
				</form>
			</div>
				
			
			<div id="content-extractor" style="display:none;">
				<a id="wpsp-select-html" class="button-primary">Add selected content to my post</a>
				<iframe id="content-extractor-iframe" name="wpsp-extractor"></iframe>
			</div>
			<div class="overlay-loading" style="display:none;"></div>';
				} elseif ($template == 'extract') {
					if ($page) : 
						echo $page; 
					else:
						echo '<p>Error loading page</p>';
					endif;
				} elseif ($template == 'auto') {
					if ($page) : 
						echo $page; 
					else:
						echo '<p>Error loading page</p>';
					endif;
				}
				
		} else {

        if (!$vars || !count($vars)) {
            $vars = WpScraperPro::$templateVariables;
        }
        if (isset($vars)) {
            extract($vars);
        }
		if ($template == 'add') {
			$post_type_options = '';
			$args = array(
			   'public'   => true,
			);
			foreach ( get_post_types( $args, 'names' ) as $post_type ) {
			   if ($post_type == 'attachment') continue;
			   if ($post_type == 'post') {
				   $selected = 'selected="selected"';
			   } else $selected = '';
			   $post_type_options .= '<option value="'.$post_type.'"'.$selected.'>' . ucfirst($post_type) . '</p>';
			}
			echo '<div id="post-body" class="wrap wpsp-form">';
	if(isset($_GET['pid'])) {
		$pid = $_GET['pid'];
	$view = get_permalink($pid);
	$edit = get_edit_post_link($pid);
    echo '<div id="message" class="updated notice is-dismissible">
        <p>
		Post created
		<a style="padding-left: 5px;" target="_blank" href="'.$view.'">View Post</a>
		<a style="padding-left: 5px;" target="_blank" class="post-edit-link" href="'.$edit.'">Edit Post</a>
		</p>
    </div>';
	}
    echo '<h2>Add New Scraped Post</h2>

    <form method="post" action="'.wp_scraper_pro_url('wp-scraper-pro', 'add').'" id="wpsp-add-post-form">
        <input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'extract').'" id="wpsp-content-extractor-url" />
		<input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'downloader').'" id="wpsp-downloader-url" />';
        wp_nonce_field( 'wpsp-save-wpscraper'); 
        echo '<div id="wpsp-add-source-form-container" class="metabox-holder">
			<div id="wpsp-extractor-box">
                <label for="wpsp-url"><b>Url to Scrape:</b></label>
				<div class="field wpsp-field-container">
					<input id="wpsp-url" class="regular-text ltr" name="url"  style="width: 100%;" />
				</div>
            </div>
			<div id="add_wpsp_post_container">
            <div id="postbox-container-1" class="postbox-container">
			<div id="titlediv" class="wpsp-field-container">
                <input type="text" name="title" value="" id="title" spellcheck="true" placeholder="Enter post title" />
				<a id="choose_title_content" title="Click to select content you want to use for the title. Then click the button below to add it to the title field." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button post-select-btn">Choose Title</a>
            </div>

            <div id="wpsp-data" class="wpsp-meta-box-container meta-box-sortables">
                <div class="postbox">
                    <h3 class="hndle"><span>Post Content</span></h3>
					
                    <div class="inside">
                        <div class="field wpsp-field-container">';
						add_action('media_buttons_context', 'wpsp_body_button');
                        wp_editor( '', 'wpsp-html' );
                            echo '<input type="hidden" id="wpsp-images" name="images" />
                        </div>
                    </div>
                </div>
            </div>
			<div id="custom-post-fields"></div>
			</div>
			<div id="postbox-container-2" class="postbox-container">
			<div id="submitdiv" class="postbox">
				<div class="handlediv" title="Click to toggle">
				<br>
				</div>
				<h3 class="hndle ui-sortable-handle">
				<span>Publish</span>
				</h3>
				<div class="inside">
				<div id="submitpost" class="submitbox">
				<div id="minor-publishing">
				<div id="misc-publishing-actions">
				<div class="misc-pub-section misc-pub-post-type">
				<label for="post_type">Post Type:</label>
				<span id="post-type-display">Post</span>
				<a class="edit-post-type hide-if-no-js" href="#post_type" style="display: inline;">
				<span aria-hidden="true">Edit</span>
				<span class="screen-reader-text">Edit type</span>
				</a>
				<div id="post-type-select" class="hide-if-js" style="display: none;">
				<input id="hidden_post_type" type="hidden" value="post" name="hidden_post_type">
				<select id="post_type" name="post_type">'.$post_type_options.'</select>
				<a class="save-post-type hide-if-no-js button" href="#post_type">OK</a>
				<a class="cancel-post-type hide-if-no-js button-cancel" href="#post_type">Cancel</a>
				</div>
				</div>
				<div class="misc-pub-section misc-pub-post-status">
				<label for="post_status">Status:</label>
				<span id="post-status-display">Published</span>
				<a class="edit-post-status hide-if-no-js" href="#post_status" style="display: inline;">
				<span aria-hidden="true">Edit</span>
				<span class="screen-reader-text">Edit status</span>
				</a>
				<div id="post-status-select" class="hide-if-js" style="display: none;">
				<input id="hidden_post_status" type="hidden" value="publish" name="hidden_post_status">
				<select id="post_status" name="post_status">
				<option value="publish" selected="selected">Published</option>
				<option value="pending">Pending Review</option>
				<option value="draft" selected="selected">Draft</option>
				</select>
				<a class="save-post-status hide-if-no-js button" href="#post_status">OK</a>
				<a class="cancel-post-status hide-if-no-js button-cancel" href="#post_status">Cancel</a>
				</div>
				</div>
				<div class="clear"></div>
				</div>
				</div>
				</div>
				</div>
				<div id="major-publishing-actions">
					<div class="save-wpscraper-form">
						<input type="submit" class="button-primary" name="save" value="Save Post" />
					</div>
				</div>
			</div>
			<div id="extract-options" class="postbox">
				<div class="handlediv" title="Click to toggle">
				<br>
				</div>
				<h3 class="hndle ui-sortable-handle">
				<span>Extract Options</span>
				</h3>
				<div class="inside">
				<label class="extract-opt" for="js" style="display: inline-block"><input id="js" type="checkbox" name="js" value="1">Load JavaScript</label><p class="description">Some content may need javascript enabled to display correctly. Check this box to enable javascript while selecting content.</p><br>
				<label class="extract-opt" for="down" style="display: inline-block"><input id="down" type="checkbox" name="down" value="1">Load Restricted Image Content</label><p class="description">Some images will not load due to cross domain conflicts. Use this feature to load these restricted images. However, it doesn\'t work with all server configurations. Use with caution.</p>
				</div>
			</div>
			<div id="submitdiv" class="postbox">
				<div class="handlediv" title="Click to toggle">
				<br>
				</div>
				<h3 class="hndle ui-sortable-handle">
				<span>Post Options</span>
				</h3>
				<div class="inside">
					<label class="misc-pub-section" for="incimages" style="display: inline-block"><input id="incimages" type="checkbox" name="incimages" value="add" checked="checked">Include Images</label><br>
					<label class="misc-pub-section" for="incvideos" style="display: inline-block"><input id="incvideos" type="checkbox" name="incvideos" value="add" checked="checked">Include Videos</label><br>
					<label class="misc-pub-section" for="inctables" style="display: inline-block"><input id="inctables" type="checkbox" name="inctables" value="add" checked="checked">Format Tables</label><br>
					<label class="misc-pub-section" for="remove_links" style="display: inline-block"><input id="remove_links" type="checkbox" name="remove_links" value="remove" >Remove Links</label><br>
					<label class="misc-pub-section" for="add_copy" style="display: inline-block"><input id="add_copy" type="checkbox" name="add_copy" value="add" >Add source link to the content</label><br>
					<hr>
					<label class="misc-pub-section" for="htmlelems" style="display: inline-block">HTML to Include:</label><br/>
					<label class="misc-pub-section"><input type="radio" name="htmlelems" value="none" checked >Strip All HTML Elements</label><br><br>
					<label class="misc-pub-section"><input type="radio" name="htmlelems" value="all" >Include All Post HTML Elements</label><br><br>
					<label class="misc-pub-section"><input type="radio" name="htmlelems" value="simple" >Include Basic HTML Elements</label><br><br>
					<label class="misc-pub-section"><input type="radio" name="htmlelems" value="specific" >Include Specific HTML Elements</label><br>
					<textarea id="wpsp-elements" rows="3" cols="27" class="wpsp-elements" name="elements" style="margin: 10px; width:95%;" disabled >br, b, em, strong, mark, i, u, div, h1, h2, h3, h4, h5, h6, li, p, span, ul, ol</textarea>
				</div>
			</div>';
			do_meta_boxes('toplevel_page_wp-scraper-pro', 'side', '');
			echo '</div>
        </div>
        </div>
    </form>
</div>

<div id="content-extractor" style="display:none;">
	<a id="wpsp-select-html" class="button-primary">Add selected content to my post</a>
    <iframe id="content-extractor-iframe" name="wpsp-extractor"></iframe>
</div>';
		} elseif ($template == 'extract') {
			if ($page) : 
    			echo $page; 
			else:
		    	echo '<p>Error loading page</p>';
			endif;
		} elseif ($template == 'auto') {
			if ($page) : 
    			echo $page; 
			else:
		    	echo '<p>Error loading page</p>';
			endif;
		}
		}
        //include(WPSP_DIR.'/templates/'.$template.'.phtml');
    }
	
	public static function wp_scraper_pro_add_content(){
        $data = $_POST;
        if (isset($data['_wpnonce'])) unset($data['_wpnonce']);
        if (isset($data['_wp_http_referer'])) unset($data['_wp_http_referer']);
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $ajaxRequest = true;
        } else {
            $ajaxRequest = false;
        }
		
        if (!empty($data) && $ajaxRequest) {
            check_admin_referer( 'wpsp-save-wpscraper' );
		
				if (isset($data['url']) && $data['url']) {
					WpScraperPro::$url = $data['url'];
				}
				
				if (isset($data['wpsp-html']) && $data['wpsp-html']) {
					WpScraperPro::$html = stripslashes($data['wpsp-html']);
				}
				
				if (isset($data['images']) && $data['images']) {
					WpScraperPro::$images = $data['images'];
				}
				
				if (isset($data['htmlelems'])) {
					if ($data['htmlelems'] == 'all') {
						$tags = wp_kses_allowed_html( 'post' );
					} elseif ($data['htmlelems'] == 'simple') {
						$tags = array
								(
									'br' => array(),
									'b' => array(),
									'em' => array(),
									'strong' => array(),
									'mark' => array(),
									'i' => array(),
									'u' => array(),
									'col' => array
										(
											'span' => array(),
										),
									'colgroup' => array
										(
											'span' => array(),
										),
									'div' => array(),
									'h1' => array(),
									'h2' => array(),
									'h3' => array(),
									'h4' => array(),
									'h5' => array(),
									'h6' => array(),
									'img' => array
										(
											'alt' => array(),
											'src' => array(),
										),
									'li' => array(),
									'p' => array(),
									'span' => array(),
									'table' => array(),
									'tbody' => array(),
									'td' => array
										(
											'colspan' => array(),
											'rowspan' => array(),
										),
									'tfoot' => array(),
									'th' => array
										(
											'colspan' => array(),
											'rowspan' => array(),
										),
									'thead' => array(),
									'tr' => array(),
									'ul' => array(),
									'ol' => array(),
								);
					} else {
						$tags = array();
						
						if (isset($data['incimages']) && $data['incimages'] == 'add') {
							$tags['img'] = array
										(
											'alt' => array(),
											'src' => array(),
										);
						}
						if (isset($data['inctables']) && $data['inctables'] == 'add') {
							$tags['col'] = array
										(
											'span' => array(),
										);
							$tags['colgroup'] = array
										(
											'span' => array(),
										);
							$tags['table'] = array();
							$tags['tbody'] = array();
							$tags['td'] = array
										(
											'colspan' => array(),
											'rowspan' => array(),
										);
							$tags['tfoot'] = array();
							$tags['th'] = array
										(
											'colspan' => array(),
											'rowspan' => array(),
										);
							$tags['thead'] = array();
							$tags['tr'] = array();
						}
						if ($data['htmlelems'] == 'specific' && isset($data['elements'])) {
							$elements = explode(', ', $data['elements']);
							foreach ($elements as $elem) {
								$tags[$elem] = array();	
							}
						}
					}
				if(isset($data['remove_links']) && $data['remove_links'] == 'remove') {
					
				unset($tags['a']);
				
				} 
				if (isset($data['incvideos']) && $data['incvideos'] == 'add') {
						$tags['iframe'] = array(
							'id' => array(),
							'title' => array(),
							'src' => array(),
							'allowfullscreen' => array(),
							'width' => array(),
							'height' => array(),
							'name' => array(),
						);
					}
				
				
			WpScraperPro::$html = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', WpScraperPro::$html );
			WpScraperPro::$html = wp_kses(WpScraperPro::$html, $tags);
			}
			
				
				$excerpt = wp_strip_all_tags(WpScraperPro::$html);
				$excerpt = wp_trim_words($excerpt, 55, ' [...]');
				
				$category = '';
				if (isset($data['post_category'])) {
					if (!is_array($data['post_category'])) {
						if (strpos($data['post_category'], ',') == false) {
							$cat_id = wp_create_category($data['post_category']);
							$category = array($cat_id);
						} elseif (strpos($data['post_category'], ',') !== false) {
							$cats = substr($data['post_category'], 1);
							$category = explode(',', $cats);
						}
					} else $category = $data['post_category'];
				}
				
				$title = $data['title'];
				
				if (isset($data['title_prefix'])) {
					$title = $data['title_prefix'].$title;
				}
				
				if (isset($data['title_suffix'])) {
					$title = $title.$data['title_suffix'];
				}
				
				$tags = '';
				if (isset($data['tax_input-post_tag'])) $tags = $data['tax_input-post_tag'];
				
				if (isset($data['add_copy'])) {
					if($data['add_copy'] == 'add') {
						$curHtml = WpScraperPro::$html;
						$copy = '<br><p class="wpss_copy">Content retrieved from: <a href="'.WpScraperPro::$url.'" target="_blank">'.WpScraperPro::$url.'</a>.</p>';
						WpScraperPro::$html = $curHtml.$copy;	
					}
				}
				
				$postId = wp_insert_post(
					array(
						'post_type' => $data['hidden_post_type'],
						'post_status' => $data['hidden_post_status'],
						'post_title' => $title,
						'post_content' => WpScraperPro::$html,
						'post_excerpt' => $excerpt,
						'post_category' => $category,
						'tags_input' => $tags
					)
				);		
				
				if (WpScraperPro::$images) {
					$images = explode("\n", WpScraperPro::$images);
		
					foreach ($images as $im) {
						$origSrc = $src = trim($im);
		
						$parts = parse_url($src);
						if (isset($parts['query']) && $parts['query']) {
							parse_str($parts['query'], $query);
							if (isset($query['action']) && ($query['action']=='downloader')) {
								$src = urldecode($query['url']);
							}
						}
		
						if (substr($src, 0, 2) == '//') {
							$src = 'http:'. $src;
						}
		
						// Download to temp folder
						$tmp = download_url( $src );
						$file_array = array();
						$newSrc = '';
		
						preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $src, $matches);
						if (isset($matches[0]) && $matches[0]) {
							$file_array['name'] = basename($matches[0]);
							$file_array['tmp_name'] = $tmp;
							if ( is_wp_error( $tmp ) ) {
								@unlink($file_array['tmp_name']);
								$file_array['tmp_name'] = '';
							} else {
								// do the validation and storage stuff
								$imageId = media_handle_sideload( $file_array, $postId, '');
		
								// If error storing permanently, unlink
								if ( is_wp_error($imageId) ) {
									@unlink($file_array['tmp_name']);
								} else {
									$newSrc = wp_get_attachment_url($imageId);
									update_post_meta( $imageId, '_wpsp_parent', $postId );
								}
							}
						} else {
							@unlink($tmp);
						}
		
						// Replace images url in code
						if ($newSrc) {
							WpScraperPro::$html = str_replace(htmlentities($origSrc), $newSrc, WpScraperPro::$html);
						}
		
					}
				} 
				
				if($data['featured_image']) {
				$feat_image = $data['featured_image'];
				if (is_numeric($feat_image)) {
					$thumb_id = $feat_image;
				} else {
					$origSrc = $src = trim($data['featured_image']);
	
					$parts = parse_url($src);
					if (isset($parts['query']) && $parts['query']) {
						parse_str($parts['query'], $query);
						if (isset($query['action']) && ($query['action']=='downloader')) {
							$src = urldecode($query['url']);
						}
					}
	
					if (substr($src, 0, 2) == '//') {
						$src = 'http:'. $src;
					}
	
					// Download to temp folder
					$tmp = download_url( $src );
					$file_array = array();
					$newSrc = '';
	
					preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $src, $matches);
					if (isset($matches[0]) && $matches[0]) {
						$file_array['name'] = basename($matches[0]);
						$file_array['tmp_name'] = $tmp;
						if ( is_wp_error( $tmp ) ) {
							@unlink($file_array['tmp_name']);
							$file_array['tmp_name'] = '';
						} else {
							// do the validation and storage stuff
							$imageId = media_handle_sideload( $file_array, $postId, '');
	
							// If error storing permanently, unlink
							if ( is_wp_error($imageId) ) {
								@unlink($file_array['tmp_name']);
							} else {
								$newSrc = wp_get_attachment_url($imageId);
								update_post_meta( $imageId, '_wpsp_parent', $postId );
								$thumb_id = $imageId;
							}
						}
					} else {
						@unlink($tmp);
					}
		
				} 
				} else $thumb_id = '';
				

                $url = WpScraperPro::$url;
				
				$meta = get_post_meta($postId);
				foreach ($meta as $key=>$item) {
					delete_post_meta($postId, $key);
				}
				
				$postId = wp_update_post(
					array(
						'ID' => (int) $postId,
						'post_type' => $data['hidden_post_type'],
						'post_status' => $data['hidden_post_status'],
						'post_title' => $title,
						'post_content' => WpScraperPro::$html,
						'post_excerpt' => $excerpt,
						'post_category' => $category,
						'tags_input' => $tags
					)
				);
				if ($thumb_id != '') {
				set_post_thumbnail( $postId, $thumb_id );
				}

                $redirect_url = wp_scraper_pro_url('wp-scraper-pro');
				$response['redirect_url'] = $redirect_url.'&pid='.$postId;
				$response['pid'] = $postId;
				$response['view'] = get_permalink($postId);
				$response['edit'] = get_edit_post_link($postId);
				$response = preg_replace_callback(
				'/\\\\u([0-9a-zA-Z]{4})/',
				function ($matches) {
					return mb_convert_encoding(pack('H*',$matches[1]),'UTF-8','UTF-16');
				},
				json_encode($response)
				);
				echo $response;
        		exit;
            
        }

        
	  	return array();
    }
	
	public static function wp_scraper_pro_extract_content(){
        $request = $_GET;
        $blockUrl = isset($_GET['blockUrl'])?$_GET['blockUrl']:null;
		$downloader = isset($_GET['down'])?$_GET['down']:null;
		$urllist = isset($_GET['url'])?$_GET['url']:null;
		$js = isset($_GET['js'])?true:false;

        if ($blockUrl) {
            $blockUrl = trim(urldecode($blockUrl));

            if (substr($blockUrl, 0, 2) == '//') {
                $blockUrl = 'http://' . substr($blockUrl, 2);
            } elseif (substr($blockUrl, 0, 4) != 'http') {
                $blockUrl = 'http://' . $blockUrl;
            }

            
			WpScraperPro::$url = $blockUrl;
			
					
				try {
					if (!function_exists('file_get_html')) {
						require_once(WPSP_DIR.'/includes/simple_html_dom.php');
					}
		
					$parts = parse_url($blockUrl);
					$domain = $parts['scheme'].'://'.$parts['host'];
					
					if (isset($parts['port']) && $parts['port'] && ($parts['port'] != '80')) {
						$domain .= ':'.$parts['port'];
					}
		
					// Relative path URL
					$relativeUrl = $domain;
					if (isset($parts['path']) && $parts['path']) {
						$pathParts = explode('/', $parts['path']);
						if (count($pathParts)) {
							unset($pathParts[count($pathParts)-1]);
							$relativeUrl = $domain.'/'.implode('/',$pathParts);
						}
					}
		
					$content = wp_remote_get($blockUrl);
					if (is_wp_error($content) || ($content['response']['code'] != 200)) {
						$arrContextOptions=array(
							"ssl"=>array(
								"verify_peer"=>false,
								"verify_peer_name"=>false,
							),
							'http'=>array(
								'ignore_errors' => true,
								'method'=>"GET",
								'header'=>"Accept-language: en-US,en;q=0.5\r\n" .
									"Cookie: foo=bar\r\n" .
									"User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Version/8.0.8 Safari/600.8.9\r\n" // i.e. An iPad
							)
						);
						$html = file_get_html($blockUrl, false, stream_context_create($arrContextOptions));
					} else {
						$html = str_get_html($content['body']);
					}
		
					if (!$html) return false;
		
					if (!$js) {
						foreach($html->find('script') as $element) {
							$element->outertext = '';
						}
					}
		
					// Remove meta
					foreach($html->find('meta[http-equiv*=refresh]') as $meta) {
						$meta->outertext = '';
					}
		
					// Remove meta x-frame
					foreach($html->find('meta[http-equiv*=x-frame-options]') as $meta) {
						$meta->outertext = '';
					}
		
					// Modify image and CSS URL's adding domain name if needed
					foreach($html->find('img') as $element) {
						$src = trim($element->src);
						
						if (empty($src)) $src = $src;
						elseif (strlen($src)>2 && (substr($src, 0, 1) == '/') && ((substr($src, 0, 2) != '//'))) {
							$src = $domain.$src;
						} elseif ((substr($src, 0, 4) != 'http') && (substr($src, 0, 2) != '//')) {
							$src = $relativeUrl .'/'.$src;
						}
						
						$downloader = $downloader?wp_scraper_pro_url('wp-scraper-pro', 'downloader'):'';
						
						if ($downloader) {
							if (strpos($downloader, '?')) {
								$element->src = $downloader.'&url='.wp_scraper_pro_encodeURIComponent($src);
							} else {
								$element->src = $downloader.'?url='.wp_scraper_pro_encodeURIComponent($src);
							}
						} else {
							$element->src = $src;
						}
					}
		
					// Modify links
					foreach($html->find('a') as $element) {
						$href = trim($element->href);
						if (strlen($href)>2 && (substr($href, 0, 1) == '/') && ((substr($href, 0, 2) != '//'))) {
							$href = $domain.$href;
						} elseif (substr($href, 0, 4) != 'http') {
							$href = $relativeUrl .'/'.$href;
						}
						$element->href = $href;
					}
		
					// Replace all styles URL’s
					foreach($html->find('link') as $element) {
						$src = trim($element->href);
						if (strlen($src)>2 && (substr($src, 0, 1) == '/') && ((substr($src, 0, 2) != '//'))) {
							$src = $domain.$src;
						} elseif ((substr($src, 0, 4) != 'http') && (substr($src, 0, 2) != '//')) {
							$src = $relativeUrl .'/'.$src;
						}
						$element->href = $src;
					}
		
					// Append our JavaScript and CSS
					$scripts = '<script type="text/javascript" src="'.includes_url( '/js/jquery/jquery.js' ).'"></script>';
					$scripts .= '<script type="text/javascript" src="'.plugins_url( 'includes/simpledomselector.js', __FILE__ ).'?'.time().'"></script>';
					if ($urllist == 1) {
						$scripts .= '<script type="text/javascript" src="'.plugins_url( 'includes/wp-scraper-urls.js', __FILE__ ).'?'.time().'"></script>';
					} else { $scripts .= '<script type="text/javascript" src="'.plugins_url( 'includes/wp-scraper-ingest.js', __FILE__ ).'?'.time().'"></script>';}
					$scripts .= '<style type="text/css">.wpscraper-hover {outline: 3px dotted #B2E0F0 !important; opacity: .7 !important;filter: alpha(opacity=70) !important; background-color: #B2E0F0 !important;}.wpscraper-hover-parent {background-color:#B2E0F0 !important;} .wpscraper-hover img {opacity: 0.7 !important; filter: alpha(opacity=70 !important);} .wpscraper-selected {outline: 5px solid #19A3D1 !important;background-color: #4DB8DB !important; opacity: .7 !important;filter: alpha(opacity=70) !important;} .wpscraper-selected-parent {background-color: #4DB8DB !important;} .wpscraper-selected img {opacity: 0.7 !important; filter: alpha(opacity=70) !important;}
					.wpscraper-not-hover {outline: 3px dotted #efb7b2 !important; opacity: .7 !important;filter: alpha(opacity=70) !important; background-color: #efb7b2 !important;}.wpscraper-not-hover-parent {background-color:#efb7b2 !important;} .wpscraper-not-hover img {opacity: 0.7 !important; filter: alpha(opacity=70 !important);} .wpscraper-not-selected {outline: 5px solid #d12e18 !important;background-color: #db584d !important; opacity: .7 !important;filter: alpha(opacity=70) !important;} .wpscraper-not-selected-parent {background-color: #db584d !important;} .wpscraper-not-selected img {opacity: 0.7 !important; filter: alpha(opacity=70) !important;}</style>';
		
					$html = str_replace('</body>', $scripts.'</body>', $html);
		
					$page = $html;
				} catch (PicoBlockException $e) {
					$page = false;
				}
        }

        WpScraperPro::wp_scraper_pro_page(
            array(
                'page' => $page
            )
        );
        exit;
    }
	
	public static function wp_scraper_pro_downloader_content(){
        $request = $_GET;
        $url = trim(urldecode(isset($_GET['url'])?$_GET['url']:null));

        if (substr($url, 0, 2) == '//') {
            $url = 'http://'.substr($url,2);
        }

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );


        $content = file_get_contents($url, null, stream_context_create($arrContextOptions));

        echo $content;
        exit;
    }

}



/**
 * Admin enqueue scripts
 */
function wpsp_admin_enqueue_scripts( $hook ) {
	
	if ( $hook == 'toplevel_page_wp-scraper-pro' || $hook == 'admin_page_wp-scraper-pro-add-menu' || $hook == 'wp-scraper-pro_page_wp-scraper-pro-live-menu' ){
		wp_enqueue_media();
		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'wp-scraper-pro-css', plugins_url( 'wp-scraper.css', __FILE__ ), array(), '', 'all' );
		wp_enqueue_script( 'wp-scraper-pro-js', plugins_url( 'wp-scraper.js', __FILE__), array( 'jquery' ), '', 'all' );
		wp_localize_script( 'wp-scraper-pro-js', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script('post');
    }
	if ( $hook == 'overlay_for_wp-scraper-pro'  ){
		wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'wp-scraper-pro-ingest', plugins_url( 'includes/wp-scraper-ingest.js', __FILE__ ), array( 'jquery' ), '', 'all' );
    }
	if ($hook == 'wp-scraper-pro_page_wp-scraper-pro-url-menu') {
		wp_enqueue_script( 'wp-scraper-pro-js', plugins_url( 'wp-scraper.js', __FILE__), array( 'jquery' ), '', 'all' );
		wp_localize_script( 'wp-scraper-pro-js', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_style( 'wp-scraper-pro-css', plugins_url( 'wp-scraper.css', __FILE__ ), array(), '', 'all' );
	}
	if ($hook == 'admin_page_wp-scraper-pro-results-menu') {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'wp-scraper-pro-css', plugins_url( 'wp-scraper.css', __FILE__ ), array(), '', 'all' );
		wp_enqueue_script( 'wp-scraper-pro-js', plugins_url( 'wp-scraper.js', __FILE__), array( 'jquery' ), '', 'all' );
		wp_enqueue_script( 'wp-scraper-pro-multi-js', plugins_url( 'wp-scraper-multi.js', __FILE__), array( 'jquery' ), '', 'all' );
		wp_localize_script( 'wp-scraper-pro-multi-js', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );	
	}
}

function wp_scraper_pro_url($controller, $action='', $params=array()) {
        
		$url = menu_page_url( $controller, false );
		
        if ($action) {
            $url = add_query_arg(array( 'action' => $action ), $url);
        }

        if (count($params)) {
            $url = add_query_arg($params, $url);
        }

        return $url;
    }
	
function wp_scraper_pro_encodeURIComponent($str) {
    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
    return strtr(rawurlencode($str), $revert);
}

/* Generate url page */

function wp_scraper_pro_url_page() {
	add_thickbox();
	if (isset($_POST['url_list']) && $_POST['url_list'] != '') {
		$urls = $_POST['url_list'];	
	} else $urls = '';
	echo '<div class="wrap wpsp-settings">
	<h1>Url Selection</h1>
	<p class="description">You can either generate a list of urls or paste a comma separated list of urls inside the box below. Once you have the urls you want to scrape click \'Continue\' at the bottom of the page.</p>
	
	<div id="wpsp-visual-generate" class="meta-box-sortables">
	<div class="postbox closed">
	<div class="handlediv" title="Click to toggle">
	<span class="dashicons dashicons-arrow-down"></span>
	</div>
	<input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'extract').'" id="wpsp-content-extractor-url" />
	<h3  class="hndle ui-sortable-handle">
	<span>Generate Urls with Visual Selector</span>
	</h3>
	<div class="inside wpsp-url-form">
	<table class="form-table"><tbody>
	<tr><th scope="row"><label class="label" for="wpsp_url" >Url:<span class="wpsp-req">*</span></label></th>
	<td><input id="wpsp-link-url" class="regular-text ltr" type="text" name="wpsp_url"/>
	<p id="wpsp-urld" class="description" >Set this to a url that lists all the pages you would like to scrape from.</p></td></tr>
	</tbody></table>
	<a id="choose_links" title="Click to select one of the links you wish to scrape from. The Url generator will get all links that have the same class or id." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button post-select-btn button-primary">Generate Urls</a>
	</div></div></div>
	
	<div id="wpsp-generate" class="meta-box-sortables">
	<div class="postbox closed">
	<div class="handlediv" title="Click to toggle">
	<span class="dashicons dashicons-arrow-down"></span>
	</div>
	<h3  class="hndle ui-sortable-handle">
	<span>Generate Urls with PHP Crawler</span>
	</h3>
	<div class="inside wpsp-url-form wpsp-php-urls">
	<table class="form-table"><tbody>
	<tr><th scope="row"><label class="label" for="wpsp_url" >Url:<span class="wpsp-req">*</span></label></th>
	<td><input id="wpsp-url" class="regular-text ltr" type="text" name="wpsp_url"/>
	<p id="wpsp-urld" class="description" >Set this to the url you would like to pull content from.</p></td></tr>
	<tr><th scope="row"><label class="label" for="wpsp_follow" >Domain Pattern:<span class="wpsp-req">*</span></label></th>
	<td><label><input type="radio" name="wpsp_follow" value="1" >Only follow links with the same url.<p class="description">www.example.com and sub.example.com</p></label><br/>
	<label><input type="radio" name="wpsp_follow" value="2" >Only follow links with the same domain.<p class="description">www.example.com not sub.example.com</p></label><br/>
	<label><input type="radio" name="wpsp_follow" value="3" >Only follow links in the same path as the given url.<p id="wpsp-pattern" class="description">If the url is www.example.com/path/index.html, only get urls in www.example.com/path/</p></label></td></tr>
	<tr><th scope="row"><label class="label" for="wpsp_number" >Number of Pages:<span class="wpsp-req">*</span></label></th>
	<td><label><input type="radio" name="wpsp_number" value="10" >10</label><br/>
	<label><input type="radio" name="wpsp_number" value="25" >25</label><br/>
	<label><input type="radio" name="wpsp_number" value="50" >50</label><br/>
	<label><input type="radio" name="wpsp_number" value="75" >75</label><br/>
	<label><input type="radio" name="wpsp_number" value="100" >100</label>
	<p id="wpsp-num" class="description" >This sets the amount of webpages to pull from the url.</p></td></tr>
	<tr><th scope="row"><label class="label" for="wpsp_skip" >Skip Links:</label></th>
	<td><input id="wpsp-skip" type="text" name="wpsp_skip" />
	<p class="description" >Optionally skip a certain number of links. This is useful if you have already scraped a number of links from a website and want to scrape more pages now. For example, if you already created posts with 10 links from this url, and now you want to grab the next 10 links, you would enter 10 into the box above.</p></td></tr>
	<tr><th scope="row"><label class="label" for="wpsp_depth" >Depth Limit:</label></th>
	<td><input id="wpsp-depth" type="text" name="wpsp_depth" />
	<p class="description" >Optionally set the depth limit for crawling pages. If this value is set to 1, it will only gather webpages that are linked on the entry page. If it is set to 2, it will also gather all webpages linked to the pages found on the entry page.</p></td></tr>
	<tr><th scope="row"><label class="label" for="wpsp_delay" >Request Delay:</label></th>
	<td><input id="wpsp-delay" class="regular-text ltr" type="text" name="wpsp_delay" /> seconds
	<p class="description" >Optionally delay each request to the url. This can keep your site from making too many requests at once to the url. </p></td></tr>
	<tr><th scope="row"><label class="label" for="wpsp_pattern" >Path Matching:</label></th>
	<td> <select id="wpsp-typematch">
	<option value="contains">Contains</option>
	<option value="ends">Ends With</option>
	</select>
	<input id="wpsp-pattern" class="regular-text ltr" type="text" name="wpsp_pattern" />
	<p class="description" >Optionally add a word to match within urls.<br>For example, choosing "contains foo" above would only add webpages to the list that have "foo" in the path such as example.com/foo or example.com/path/this-page-has-foo</p></td></tr>
	</tbody></table>
	<p class="submit">
	<input id="wpsp-crawl-submit" class="button button-primary" type="submit" value="Get Webpages" name="submit">
	</p></div></div></div>
	
	<h3>Webpages to Scrape:</h3>
	<p class="description">Every url listed in the box below will be used to generate content for your site. Remove any generated urls that you do not want to pull content from.</p>
	<form id="wpsp-url-submit" action="'.admin_url().'admin.php?page=wp-scraper-pro-add-menu" method="POST" >
	<textarea id="wpsp-url-list" style="width: 100%; min-height: 300px;" name="url_list">'.$urls.'</textarea>
	<p class="submit">
	<input id="wpsp-continue-submit" class="button button-primary" type="submit" value="Continue" name="submit">
	</p></form>
	<div id="content-extractor" style="display:none;">
	<a id="wpsp-select-urls" class="button-primary">Generate Url List</a>
    <iframe id="content-extractor-iframe" name="wpsp-extractor"></iframe>
	</div>';
}


/* Generate results page */

function wp_scraper_pro_results_page() {	
	echo '<div class="wrap wpsp-settings">
	<div id="wpsp-data" class="loading wpsp-form">
<form id="wpsp-add-multi-post-form" class="hidden" action="'.wp_scraper_pro_url('wp-scraper-pro', 'add').'">
		<input type="hidden" value="'.(isset($_POST['wpsp-url-list'])?$_POST['wpsp-url-list']:'').'" id="wpsp-url-list" name="wpsp-url-list" />
		<input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'auto').'" id="wpsp-content-auto-url" />
		<input type="hidden" value="'.wp_scraper_pro_url('wp-scraper-pro', 'extract').'" id="wpsp-content-extractor-url" />
		<input type="hidden" value="true" id="wpsp_is_mult" />
		<input type="hidden" id="_wpnonce" name="_wpnonce" value="'.(isset($_POST['_wpnonce'])?$_POST['_wpnonce']:'').'" />
		<input type="hidden" name="_wp_http_referer" value="'.(isset($_POST['_wp_http_referer'])?$_POST['_wp_http_referer']:'').'" />
		<input id="wpsp-url" class="regular-text ltr" name="url" value="'.(isset($_POST['url'])?$_POST['url']:'').'" />
		<input class="wpsp-selector" type="text" name="title_selector" value="'.(isset($_POST['title_selector'])?$_POST['title_selector']:'').'" id="title_selector" />
		<input type="text" name="title_prefix" size="80" value="'.(isset($_POST['title_prefix'])?$_POST['title_prefix']:'').'" id="title_prefix" spellcheck="true" placeholder="Prefix" />
		<input type="text" name="title" size="80" value="'.(isset($_POST['title'])?$_POST['title']:'').'" id="title" spellcheck="true" placeholder="Enter post title" />
		<input type="text" name="title_suffix" size="80" value="'.(isset($_POST['title_suffix'])?$_POST['title_suffix']:'').'" id="title_suffix" spellcheck="true" placeholder="Suffix" />
		<input class="wpsp-selector" type="text" name="body_selector" value="'.(isset($_POST['body_selector'])?$_POST['body_selector']:'').'" id="body_selector" />
		<textarea class="wp-editor-area" rows="20" autocomplete="off" cols="40" name="wpsp-html" id="wpsp-html">'.(isset($_POST['wpsp-html'])?$_POST['wpsp-html']:'').'</textarea>
		<input type="hidden" id="wpsp-images" name="images" value="'.(isset($_POST['images'])?$_POST['images']:'').'" />
		<input id="hidden_post_type" type="hidden" value="'.(isset($_POST['hidden_post_type'])?$_POST['hidden_post_type']:'').'" name="hidden_post_type">
		<input id="hidden_post_status" type="hidden" value="'.(isset($_POST['hidden_post_status'])?$_POST['hidden_post_status']:'').'" name="hidden_post_status">
		
		<label class="misc-pub-section" for="incimages" style="display: inline-block"><input id="incimages" type="checkbox" name="incimages" value="add" ';
		echo (isset($_POST['incimages'])?'checked="checked"':'');
		echo '>Include Images</label><br>
		<label class="misc-pub-section" for="incvideos" style="display: inline-block"><input id="incvideos" type="checkbox" name="incvideos" value="add" ';
		echo (isset($_POST['incvideos'])?'checked="checked"':'');
		echo '>Include Videos</label><br>
		<label class="misc-pub-section" for="inctables" style="display: inline-block"><input id="inctables" type="checkbox" name="inctables" value="add" ';
		echo (isset($_POST['inctables'])?'checked="checked"':'');
		echo '>Format Tables</label><br>
		<label class="misc-pub-section" for="remove_links" style="display: inline-block"><input id="remove_links" type="checkbox" name="remove_links" value="remove" ';
		echo (isset($_POST['remove_links'])?'checked="checked"':'');
		echo '>Remove Links</label><br>
		<label class="misc-pub-section" for="add_copy" style="display: inline-block"><input id="add_copy" type="checkbox" name="add_copy" value="add" ';
		echo (isset($_POST['add_copy'])?'checked="checked"':'');
		echo '>Add source link to the content</label><br>
		<label class="misc-pub-section" for="fix" style="display: inline-block"><input id="fix" type="checkbox" name="fix" value="1" ';
		echo (isset($_POST['fix'])?'checked="checked"':'');
		echo '>Add Prefix and Suffix to all titles</label><br>
		<hr>
		<label class="misc-pub-section" for="htmlelems" style="display: inline-block">HTML to Include:</label><br/>
		<label class="misc-pub-section"><input type="radio" name="htmlelems" value="none" ';
		echo ( isset($_GET['htmlelems']) && $_GET['htmlelems']=='none'?'checked':'');
		echo '>Strip All HTML Elements</label><br><br>
		<label class="misc-pub-section"><input type="radio" name="htmlelems" value="all" ';
		echo (isset($_GET['htmlelems']) && $_GET['htmlelems']=='all'?'checked':'');
		echo '>Include All Post HTML Elements</label><br><br>
		<label class="misc-pub-section"><input type="radio" name="htmlelems" value="simple" ';
		echo (isset($_GET['htmlelems']) && $_GET['htmlelems']=='simple'?'checked':'');
		echo '>Include Basic HTML Elements</label><br><br>
		<label class="misc-pub-section"><input type="radio" name="htmlelems" value="specific" ';
		echo (isset($_GET['htmlelems']) && $_GET['htmlelems']=='specific'?'checked':'');
		echo '>Include Specific HTML Elements</label><br>
		<textarea id="wpsp-elements" rows="3" cols="27" class="wpsp-elements" name="elements" style="margin: 10px; width:95%;">'.(isset($_POST['elements'])?$_POST['elements']:'').'</textarea><br><br>
		<hr>
		<label class="misc-pub-section" for="wpspinterval" style="display: inline-block">Time Delay Between Scrapes</label><br/>
		<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="0" ';
		echo ( isset($_GET['wpspinterval']) && $_GET['wpspinterval']=='0'?'checked':'');
		echo '>None</label><br><br>
		<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="10000" ';
		echo ( isset($_GET['wpspinterval']) && $_GET['wpspinterval']=='10000'?'checked':'');
		echo '>Ten Seconds</label><br><br>
		<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="30000"';
		echo ( isset($_GET['wpspinterval']) && $_GET['wpspinterval']=='30000'?'checked':'');
		echo ' >Thirty Seconds</label><br><br>
		<label class="misc-pub-section"><input type="radio" name="wpspinterval" value="60000" ';
		echo ( isset($_GET['wpspinterval']) && $_GET['wpspinterval']=='60000'?'checked':'');
		echo '>One Minute</label>
		
    	<input class="wpsp-selector" type="text" name="cat_selector" value="'.(isset($_POST['cat_selector'])?$_POST['cat_selector']:'').'" id="cat_selector" />
		
		<input class="post-category" value="'.(isset($_POST['post_category'])?(is_array($_POST['post_category'])?implode(',',$_POST['post_category']):$_POST['post_category']):'').'" name="post_category" />
				
				
		
        <input class="wpsp-selector" type="text" name="tags_selector" value="'.(isset($_POST['tags_selector'])?$_POST['tags_selector']:'').'" id="tags_selector" />
		
		<textarea class="the-tags" name="tax_input-post_tag">'.(isset($_POST['tax_input-post_tag'])?$_POST['tax_input-post_tag']:'').'</textarea>
		
		<input class="wpsp-selector" type="text" name="fi_selector" value="'.(isset($_POST['fi_selector'])?$_POST['fi_selector']:'').'" id="fi_selector" />
		<input id="wpsp_featured_image" type="hidden" name="featured_image" value="'.(isset($_POST['featured_image'])?$_POST['featured_image']:'').'" />
		</form>
	</div>
	<div id="wpsp-scraper-results" style="display: none;">
	<h2>WP Scraper Results</h2>
	<p class="description">Please remain on this page until all of your pages have been created. This process may take several minutes. As new posts are created they will be shown in the table below.</p><table class="wp-list-table widefat fixed striped posts">
<thead>
<tr>
<th id="title" class="manage-column column-title column-primary" scope="col">Title</th>
<th id="view" class="manage-column column-view" scope="col">View</th>
<th id="edit" class="manage-column column-edit" scope="col">Edit</th>
</tr>
</thead>
<tbody id="the-list">
</tbody>
</table>
</div>
<form id="retry" style="display:none;" action="'.admin_url().'admin.php?page=wp-scraper-pro-url-menu" method="POST"><br><br><p class="description">Below are the urls that failed to scrape. There are usually only two reasons that a page fails to scrape. The first is if your php allowed memory size is too small to handle the scrape. You can change your php.ini settings to allow for a higher memory_limit. The other main reason scrapes fail is from the selector not being exactly the same on all pages. If this is the case simply rescrape the remaining pages with new selectors. <a id="tryagain" href="#">Click here to try again.</a></p>
<p style="font-weight:bold">If you consistently receive multiple errors, try setting the "Time Delay Between Scrapes" under Extract Options.</p>
<textarea id="url_list" name="url_list" style="width:100%;height:150px;"></textarea></form>

<div id="content-extractor-auto" style="display:none;">
	<iframe id="content-extractor-auto-iframe" name="wpsp-extractor-auto"></iframe>
</div>';
}

add_action( 'wp_ajax_wpsp_live_scrape_action', 'wp_scraper_pro_live_scrape_action');
function wp_scraper_pro_live_scrape_action($url = '', $selector = '', $downloader = '', $tags = ''){
	$multi = false;
		if (empty($url) && empty($selector) && empty($downloader)) {
			if (isset($_REQUEST['url'])) {
				$url = $_REQUEST['url'];
			} else die(json_encode(array('message' => 'ERROR', 'code' => 1329)));
			if (isset($_REQUEST['selector'])) {
				$selector = $_REQUEST['selector'];
			} else die(json_encode(array('message' => 'ERROR', 'code' => 1330)));
			if (isset($_REQUEST['downloader'])) {
				$downloader = $_REQUEST['downloader'];
			} else die(json_encode(array('message' => 'ERROR', 'code' => 1331)));
			if (isset($_REQUEST['tags'])) {
				$tags = $_REQUEST['tags'];
			} else die(json_encode(array('message' => 'ERROR', 'code' => 1332)));
		} else {$multi = true;}
		
set_time_limit(10000);

if (!function_exists('file_get_html')) {
	require_once(WPSP_DIR.'/includes/simple_html_dom.php');
}
$lb = "<br/>";

if (strpos($selector, ':not') !== false) {
	$selectorArr = explode(' :not', $selector);
	$deselector = array_pop($selectorArr);
	rtrim($deselector,",");
	$selector = $selectorArr[0];
} else {$deselector = "";}

$url = trim(urldecode($url));

if (substr($url, 0, 2) == '//') {
	$url = 'http://' . substr($url, 2);
} elseif (substr($url, 0, 4) != 'http') {
	$url = 'http://' . $url;
}

	$parts = parse_url($url);
	$domain = $parts['scheme'].'://'.$parts['host'];

	if (isset($parts['port']) && $parts['port'] && ($parts['port'] != '80')) {
		$domain .= ':'.$parts['port'];
	}

	// Relative path URL
	$relativeUrl = $domain;
	if (isset($parts['path']) && $parts['path']) {
		$pathParts = explode('/', $parts['path']);
		if (count($pathParts)) {
			unset($pathParts[count($pathParts)-1]);
			$relativeUrl = $domain.'/'.implode('/',$pathParts);
		}
	}

	$content = wp_remote_get($url);
	if (is_wp_error($content) || ($content['response']['code'] != 200)) {
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
			'http'=>array(
				'ignore_errors' => true,
				'method'=>"GET",
				'header'=>"Accept-language: en-US,en;q=0.5\r\n" .
					"Cookie: foo=bar\r\n" .
					"User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Version/8.0.8 Safari/600.8.9\r\n" // i.e. An iPad
			)
		);
		$html = file_get_html($url, false, stream_context_create($arrContextOptions));
	} else {
		$html = str_get_html($content['body']);
	}

	if (!$html) return false;

	// Remove meta
	foreach($html->find('meta[http-equiv*=refresh]') as $meta) {
		$meta->outertext = '';
	}

	// Remove meta x-frame
	foreach($html->find('meta[http-equiv*=x-frame-options]') as $meta) {
		$meta->outertext = '';
	}

	// Modify image and CSS URL's adding domain name if needed
	foreach($html->find('img') as $element) {
		$src = trim($element->src);
		if (strlen($src)>2 && (substr($src, 0, 1) == '/') && ((substr($src, 0, 2) != '//'))) {
			$src = $domain.$src;
		} elseif ((substr($src, 0, 4) != 'http') && (substr($src, 0, 2) != '//')) {
			$src = $relativeUrl .'/'.$src;
		}

		if ($downloader != 'false') {
			if (strpos($downloader, '?')) {
				$element->src = $downloader.'&url='.wp_scraper_pro_encodeURIComponent($src);
			} else {
				$element->src = $downloader.'?url='.wp_scraper_pro_encodeURIComponent($src);
			}
		} else {
			$element->src = $src;
		}
	}

	// Modify links
	foreach($html->find('a') as $element) {
		$href = trim($element->href);
		if (strlen($href)>2 && (substr($href, 0, 1) == '/') && ((substr($href, 0, 2) != '//'))) {
			$href = $domain.$href;
		} elseif (substr($href, 0, 4) != 'http') {
			$href = $relativeUrl .'/'.$href;
		}
		$element->href = $href;
	}

	// Replace all styles URL’s
	foreach($html->find('link') as $element) {
		$src = trim($element->href);
		if (strlen($src)>2 && (substr($src, 0, 1) == '/') && ((substr($src, 0, 2) != '//'))) {
			$src = $domain.$src;
		} elseif ((substr($src, 0, 4) != 'http') && (substr($src, 0, 2) != '//')) {
			$src = $relativeUrl .'/'.$src;
		}
		$element->href = $src;
	}
	
	foreach($html->find('script') as $element) {
		$element->outertext = '';
	}
					
if ($deselector != '') {
	if (strpos($deselector,', ') !== false) {
		$depieces = explode(', ', $deselector);	
	} else $depieces = array($deselector);
	foreach ($depieces as $dp) {
	$departs = explode(' > ', $dp);
	$i = 0;
	foreach ($departs as $deprt) {
		if ($i == 0 && strpos($deprt, ':eq(') !== false) {
			$deprtArr = explode(":eq(", $deprt);
			$deelem = $deprtArr[0];
			$deeIndex = rtrim($deprtArr[1], ")");
			$dereturnElement = $html->find($deelem, $deeIndex);
		} elseif ($i == 0 && strpos($deprt, ':eq(') == false) {
			$dereturnElement = $html->find($deprt);
		} elseif ($i > 0 && strpos($deprt, ':eq(') !== false) {
			$deprtArr = explode(":eq(", $deprt);
			$deelem = $deprtArr[0];
			$deeIndex = rtrim($deprtArr[1], ")");
			$deretArray = $dereturnElement->find($deelem);
			$k = 0;
			foreach ($deretArray as $dechild) {
				if ($dechild->tag == $deelem && $k == $deeIndex) {
					$dereturnElement = $dechild;
				}
				$k++;
			}
			
		} else {
			$dereturnElement = $dereturnElement->find($prt);
		}
		$i++;
	}
			$dereturnElement->outertext = '';
			
	}
}

$return = '';
if (strpos($selector,', ') !== false) {
	$pieces = explode(', ', $selector);	
} else $pieces = array($selector);
$wrap = false;
$j = 0;
foreach ($pieces as $p) {
$parts = explode(' > ', $p);
$i = 0;
foreach ($parts as $prt) {
	if ($i == 0 && strpos($prt, ':eq(') !== false) {
		$prtArr = explode(":eq(", $prt);
		$elem = $prtArr[0];
		$eIndex = rtrim($prtArr[1], ")");
		$returnElement = $html->find($elem, $eIndex);
		if ($elem == 'td' || $elem == 'th' || $elem == 'tr') {
			$wrap = true;
		}
	} elseif ($i == 0 && strpos($prt, ':eq(') == false) {
		$returnElement = $html->find($prt);
		if ($prt == 'td' || $prt == 'th' || $prt == 'tr') {
			$wrap = true;
		}
	} elseif ($i > 0 && strpos($prt, ':eq(') !== false) {
		$prtArr = explode(":eq(", $prt);
		$elem = $prtArr[0];
		$eIndex = rtrim($prtArr[1], ")");
		$retArray = $returnElement->find($elem);
		$k = 0;
		foreach ($retArray as $child) {
			if ($child->tag == $elem && $k == $eIndex) {
				$returnElement = $child;
			}
			$k++;
		}
		if ($elem == 'td' || $elem == 'th' || $elem == 'tr') {
			$wrap = true;
		}
	} else {
		$returnElement = $returnElement->find($prt);
		if ($prt == 'td' || $prt == 'th' || $prt == 'tr') {
			$wrap = true;
		}
	}
	$i++;
}
if ($tags == 'true') { $returnElement = $returnElement.', ';}
		
if ($returnElement == NULL && $multi) $returnElement = "ERROR";
if ($wrap == true) {
	$returnElement = '<div class="tableContent">'.$returnElement.'</div>';
}
if (!$multi) echo $returnElement;
else $return .= $returnElement;
$j++;
}
if (!$multi) wp_die();
else return $return;
}

add_action( 'wp_ajax_wpsp_get_post_id', 'wpsp_get_post_id');
function wpsp_get_post_id() {
	
	$data = $_POST;
	
	if ($data['title_selector'] != '') {
		$data['title'] = wp_scraper_pro_live_scrape_action($data['ThisUrl'], $data['title_selector'], 'false', 'false');
		if ($data['title'] == "ERROR") { die(json_encode(array('message' => 'ERROR', 'code' => 27000)));}
		$data['title'] = strip_tags($data['title']);
	}
	
	$title = $data['title'];
	error_log('title: '.$title);
			
	if (isset($data['title_prefix'])) {
		$title = $data['title_prefix'].$title;
	}
	
	if (isset($data['title_suffix'])) {
		$title = $title.$data['title_suffix'];
	}
			
	$post = get_page_by_title($title, OBJECT, 'post');
	
	if ($post == NULL) {
		die(json_encode(array('message' => 'ERROR', 'code' => 30000)));
	} else {
		
		$timezone = get_option('timezone_string');
		date_default_timezone_set($timezone);  
		
      	$time_ago = strtotime($post->post_date);  
      	$current_time = time();  
      	$time_difference = $current_time - $time_ago;  
      	$seconds = $time_difference;  
      	$minutes      = round($seconds / 60 );           // value 60 is seconds 
		
		if ($minutes <= 2) {  
	
			error_log('postId: '.$post->ID);
			
			error_log('post object: '.print_r($post,true));
			
			$response['pid'] = $post->ID;
			$response['title'] = $title;
			$response['view'] = get_permalink($post->ID);
			$response['edit'] = get_edit_post_link($post->ID);
			$response = preg_replace_callback(
			'/\\\\u([0-9a-zA-Z]{4})/',
			function ($matches) {
				return mb_convert_encoding(pack('H*',$matches[1]),'UTF-8','UTF-16');
			},
			json_encode($response)
			);
			echo $response;
			exit;
		} else {
			die(json_encode(array('message' => 'ERROR', 'code' => 40000)));
		}
	}
}

add_action( 'wp_ajax_wpsp_multi_scrape_action', 'wp_scraper_pro_multi_scrape_action');
function wp_scraper_pro_multi_scrape_action(){
	
	$data = $_POST;
	
	if (!function_exists('file_get_html')) {
		require_once(WPSP_DIR.'/includes/simple_html_dom.php');
	}
	
	if ($data['title_selector'] != '') {
		$data['title'] = wp_scraper_pro_live_scrape_action($data['ThisUrl'], $data['title_selector'], 'false', 'false');
		if ($data['title'] == "ERROR") { die(json_encode(array('message' => 'ERROR', 'code' => 27000)));}
		$data['title'] = strip_tags($data['title']);
	}
	if ($data['body_selector'] != '') {
		$data['wpsp-html'] = wp_scraper_pro_live_scrape_action($data['ThisUrl'], $data['body_selector'], 'false', 'false');
		if ($data['wpsp-html'] == "ERROR") { die(json_encode(array('message' => 'ERROR', 'code' => 27001)));}
	}
	if ($data['cat_selector'] != '') {
		$data['post_category'] = wp_scraper_pro_live_scrape_action($data['ThisUrl'], $data['cat_selector'], 'false', 'false');
		if ($data['post_category'] == "ERROR") { die(json_encode(array('message' => 'ERROR', 'code' => 27002)));}
		$data['post_category'] = strip_tags($data['post_category']);
		$categ = wp_create_category($data['post_category']);
		$data['post_category'] = array($categ);
	}
	if ($data['tags_selector'] != '') {
		$data['tax_input-post_tag'] = wp_scraper_pro_live_scrape_action($data['ThisUrl'], $data['tags_selector'], 'false', 'true');
		if ($data['tax_input-post_tag'] == "ERROR") { die(json_encode(array('message' => 'ERROR', 'code' => 27003)));}
		$data['tax_input-post_tag'] = strip_tags($data['tax_input-post_tag']);
	}
	if ($data['fi_selector'] != '') {
		$fi = wp_scraper_pro_live_scrape_action($data['ThisUrl'], $data['fi_selector'], 'false', 'false');
		if ($data['fi_selector'] == "ERROR") { die(json_encode(array('message' => 'ERROR', 'code' => 27004)));}
		$fi_html = str_get_html($fi);
		$data['featured_image'] = $fi_html->find('img', 0)->src;
	}
		if (isset($data['_wpnonce'])) unset($data['_wpnonce']);
	if (isset($data['_wp_http_referer'])) unset($data['_wp_http_referer']);
	
	if (!empty($data)) {
		
			if (isset($data['ThisUrl'])) {
				$url = $data['ThisUrl'];
			}
			
			$html = '';
			if (isset($data['wpsp-html'])) {
				$html = $data['wpsp-html'];
			}
			
			if (isset($data['htmlelems'])) {
				if ($data['htmlelems'] == 'all') {
					$tags = wp_kses_allowed_html( 'post' );
				} elseif ($data['htmlelems'] == 'simple') {
					$tags = array
							(
								'br' => array(),
								'b' => array(),
								'em' => array(),
								'strong' => array(),
								'mark' => array(),
								'i' => array(),
								'u' => array(),
								'col' => array
									(
										'span' => array(),
									),
								'colgroup' => array
									(
										'span' => array(),
									),
								'div' => array(),
								'h1' => array(),
								'h2' => array(),
								'h3' => array(),
								'h4' => array(),
								'h5' => array(),
								'h6' => array(),
								'img' => array
									(
										'alt' => array(),
										'src' => array(),
									),
								'li' => array(),
								'p' => array(),
								'span' => array(),
								'table' => array(),
								'tbody' => array(),
								'td' => array
									(
										'colspan' => array(),
										'rowspan' => array(),
									),
								'tfoot' => array(),
								'th' => array
									(
										'colspan' => array(),
										'rowspan' => array(),
									),
								'thead' => array(),
								'tr' => array(),
								'ul' => array(),
								'ol' => array(),
							);
				} else {
					$tags = array();
					
					if (isset($data['incimages'])) {
						$tags['img'] = array
									(
										'alt' => array(),
										'src' => array(),
									);
					}
					if (isset($data['inctables'])) {
						$tags['col'] = array
									(
										'span' => array(),
									);
						$tags['colgroup'] = array
									(
										'span' => array(),
									);
						$tags['table'] = array();
						$tags['tbody'] = array();
						$tags['td'] = array
									(
										'colspan' => array(),
										'rowspan' => array(),
									);
						$tags['tfoot'] = array();
						$tags['th'] = array
									(
										'colspan' => array(),
										'rowspan' => array(),
									);
						$tags['thead'] = array();
						$tags['tr'] = array();
					}
					if ($data['htmlelems'] == 'specific' && isset($data['elements'])) {
						$elements = explode(', ', $data['elements']);
						foreach ($elements as $elem) {
							$tags[$elem] = array();	
						}
					}
				}
			if(isset($data['htmlelems']) && $data['remove_links'] == 'remove') {
				unset($tags['a']);
				
			
				} 
			if (isset($data['incvideos']) && $data['incvideos'] == 'add') {
					$tags['iframe'] = array(
						'id' => array(),
						'title' => array(),
						'src' => array(),
						'allowfullscreen' => array(),
						'width' => array(),
						'height' => array(),
						'name' => array(),
					);
				}
			
		
		$html = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $html );	
		$html = wp_kses($html, $tags);
		}
			
			$excerpt = wp_strip_all_tags($html);
			$excerpt = wp_trim_words($excerpt, 55, ' [...]');
			
			$category = '';
			if (isset($data['post_category'])) {
				if (!is_array($data['post_category'])) {
					if (strpos($data['post_category'], ',') == false) {
						$cat_id = wp_create_category($data['post_category']);
						$category = array($cat_id);
					} elseif (strpos($data['post_category'], ',') !== false) {
						$cats = substr($data['post_category'], 1);
						$category = explode(',', $cats);
					}
				} else $category = $data['post_category'];
			}
			
			$title = $data['title'];
			
			if (isset($data['title_prefix'])) {
				$title = $data['title_prefix'].$title;
			}
			
			if (isset($data['title_suffix'])) {
				$title = $title.$data['title_suffix'];
			}
			
			$tags = '';
			if (isset($data['tax_input-post_tag'])) $tags = $data['tax_input-post_tag'];
			
			if (isset($url) && isset($html)) {
				if (isset($data['add_copy'])) {
					if($data['add_copy'] == 'add') {
						$curHtml = $html;
						$copy = '<br><p class="wpss_copy">Content retrieved from: <a href="'.$url.'" target="_blank">'.$url.'</a>.</p>';
						$html = $curHtml.$copy;	
					}
				}
			}
			
			$postId = wp_insert_post(
				array(
					'post_type' => $data['hidden_post_type'],
					'post_status' => $data['hidden_post_status'],
					'post_title' => $title,
					'post_content' => $html,
					'post_excerpt' => $excerpt,
					'post_category' => $category,
					'tags_input' => $tags
				)
			);
			
			if ($postId == 0 || !is_numeric($postId)) {
				die(json_encode(array('message' => 'ERROR', 'code' => 27005)));
			}
			
			$this_html = str_get_html($html);
			$imageIds = '';
			foreach ($this_html->find('img') as $image) {
				$im = $image->src;
				$origSrc = $src = trim($im);
				
				$parts = parse_url($src);
				if (isset($parts['query']) && $parts['query']) {
					parse_str($parts['query'], $query);
					if (isset($query['action']) && ($query['action']=='downloader')) {
						$src = urldecode($query['url']);
					}
				}

				if (substr($src, 0, 2) == '//') {
					$src = 'http:'. $src;
				}

				// Download to temp folder
				$tmp = download_url( $src );
				$file_array = array();
				$newSrc = '';

				preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $src, $matches);
				if (isset($matches[0]) && $matches[0]) {
					$file_array['name'] = basename($matches[0]);
					$file_array['tmp_name'] = $tmp;
					if ( is_wp_error( $tmp ) ) {
						@unlink($file_array['tmp_name']);
						$file_array['tmp_name'] = '';
					} else {
						// do the validation and storage stuff
						$imageId = media_handle_sideload( $file_array, 0, '');

						// If error storing permanently, unlink
						if ( is_wp_error($imageId) ) {
							@unlink($file_array['tmp_name']);
						} else {
							$newSrc = wp_get_attachment_url($imageId);
							if($imageIds == '')
								$imageIds = $imageId;
							else $imageIds .= ', '.$imageId;
						}
					}
				} else {
					@unlink($tmp);
				}

				// Replace images url in code
				if ($newSrc) {
					$html = str_replace($origSrc, $newSrc, $html);
				}

			}
			
			if($data['featured_image']) {
			$feat_image = $data['featured_image'];
			if (is_numeric($feat_image)) {
				$thumb_id = $feat_image;
			} else {
				$origSrc = $src = trim($data['featured_image']);

				$parts = parse_url($src);
				if (isset($parts['query']) && $parts['query']) {
					parse_str($parts['query'], $query);
					if (isset($query['action']) && ($query['action']=='downloader')) {
						$src = urldecode($query['url']);
					}
				}

				if (substr($src, 0, 2) == '//') {
					$src = 'http:'. $src;
				}

				// Download to temp folder
				$tmp = download_url( $src );
				$file_array = array();
				$newSrc = '';

				preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $src, $matches);
				if (isset($matches[0]) && $matches[0]) {
					$file_array['name'] = basename($matches[0]);
					$file_array['tmp_name'] = $tmp;
					if ( is_wp_error( $tmp ) ) {
						@unlink($file_array['tmp_name']);
						$file_array['tmp_name'] = '';
					} else {
						// do the validation and storage stuff
						$imageId = media_handle_sideload( $file_array, $postId, '');

						// If error storing permanently, unlink
						if ( is_wp_error($imageId) ) {
							@unlink($file_array['tmp_name']);
						} else {
							$newSrc = wp_get_attachment_url($imageId);
							update_post_meta( $imageId, '_wpsp_parent', $postId );
							$thumb_id = $imageId;
						}
					}
				} else {
					@unlink($tmp);
				}
	
			} 
			} else $thumb_id = '';
			
			$meta = get_post_meta($postId);
			foreach ($meta as $key=>$item) {
				delete_post_meta($postId, $key);
			}
			
			$postId = wp_update_post(
				array(
					'ID' => (int) $postId,
					'post_type' => $data['hidden_post_type'],
					'post_status' => $data['hidden_post_status'],
					'post_title' => $title,
					'post_content' => $html,
					'post_excerpt' => $excerpt,
					'post_category' => $category,
					'tags_input' => $tags
				)
			);	
			if ($thumb_id != '') {
			set_post_thumbnail( $postId, $thumb_id );
			}

			$redirect_url = wp_scraper_pro_url('wp-scraper-pro');
			$response['redirect_url'] = $redirect_url.'&pid='.$postId;
			$response['pid'] = $postId;
			$response['title'] = $title;
			$response['view'] = get_permalink($postId);
			$response['edit'] = get_edit_post_link($postId);
			$response = preg_replace_callback(
			'/\\\\u([0-9a-zA-Z]{4})/',
			function ($matches) {
				return mb_convert_encoding(pack('H*',$matches[1]),'UTF-8','UTF-16');
			},
			json_encode($response)
			);
			echo $response;
			exit;
	}
	
	return array();
}

/* Generate URL List Ajax Action */

add_action( 'wp_ajax_wpsp_ajax_scrape', 'wp_scraper_pro_scrape_site');
function wp_scraper_pro_scrape_site() {
		if (isset($_REQUEST['url'])) {
			$url = $_REQUEST['url'];
		} else die(json_encode(array('message' => 'ERROR', 'code' => 1329)));
		if (isset($_REQUEST['fol'])) {
			$fol = $_REQUEST['fol'];
		} else die(json_encode(array('message' => 'ERROR', 'code' => 1330)));
		if (isset($_REQUEST['num'])) {
			$num = $_REQUEST['num'];
		} else die(json_encode(array('message' => 'ERROR', 'code' => 1331)));
		if (isset($_REQUEST['skp'])) {
			$skp = $_REQUEST['skp'];
		}
		if (isset($_REQUEST['dep'])) {
			$dep = $_REQUEST['dep'];
		}
		if (isset($_REQUEST['del'])) {
			$del = $_REQUEST['del'];
		}
		if (isset($_REQUEST['typ'])) {
			$typ = $_REQUEST['typ'];
		}
		if (isset($_REQUEST['mat'])) {
			$mat = $_REQUEST['mat'];
		}
		
set_time_limit(10000);

include("includes/libs/PHPCrawler.class.php");

class WpScraperProCrawler extends PHPCrawler 
{
	public static $url_list;
	
	private $i = 0;
  function handleDocumentInfo(PHPCrawlerDocumentInfo $DocInfo) 
  {
    $lb = "\n";

    // Print the URL and the HTTP-status-Code
    //echo "Page requested: ".$DocInfo->url." (".$DocInfo->http_status_code.")".$lb;
    
    // Print the refering URL
    //echo "Referer-page: ".$DocInfo->referer_url.$lb;
	$skp = $_REQUEST['skp'];
    // Print if the content of the document was be recieved or not
	if($skp) {
		if( $this->i <= $skp) {
			if ($DocInfo->received == true)
			$this->i++;
		} if($this->i > $skp) {
			if ($DocInfo->received == true)
			WpScraperProCrawler::$url_list .= $DocInfo->url.",".$lb;
		}
	} else {
		if ($DocInfo->received == true)
			WpScraperProCrawler::$url_list .= $DocInfo->url.",".$lb;
	}
      //echo "Content received: ".$DocInfo->bytes_received." bytes".$lb;
    //else
		//echo "";
      //echo "Content not received".$lb; 
    
    // Now you should do something with the content of the actual
    // received page or file ($DocInfo->source), we skip it in this example 
    
    //echo $lb;
    
    flush();
  } 
}
		
$crawler = new WpScraperProCrawler();

$crawler->setURL($url);

$crawler->setFollowMode($fol);

$crawler->addContentTypeReceiveRule("#text/html#");

$crawler->obeyNoFollowTags(true);
$crawler->obeyRobotsTxt(true);
$crawler->excludeLinkSearchDocumentSections(PHPCrawlerLinkSearchDocumentSections::ALL_SPECIAL_SECTIONS);

$crawler->addURLFilterRule("#\.(jpg|jpeg|gif|png)$# i");

$crawler->enableCookieHandling(true);

if($skp) $num = $skp + $num;
$crawler->setRequestLimit($num);

if (isset($dep) && !empty($dep)) {
	$crawler->setCrawlingDepthLimit($dep);
}
if (isset($del) && !empty($del)) {
	$crawler->setRequestDelay($del);
}

/*if ($fol == 3) {
	$path = parse_url($url);
	$folders = explode('/', $path['path']);
	if (substr($path['path'], -1) != '/') {
		array_pop($folders);
	}
	$new_path = implode('/',$folders);
	echo 'new_path '.$new_path;
	if ($path['scheme'])
	$last_path = $path['scheme'].'://'.$path['host'].$new_path;
	else $last_path = $new_path;
	echo 'lastpath '.$last_path;
		if (isset($typ) && isset($mat) && !empty($mat)) {
			if ($typ == 'contains') {
				$crawler->addURLFollowRule("#^(?=.*".$last_path.")(?=.*\b".$mat."\b)$# i");
			}
			if ($typ == 'ends') {
				$crawler->addURLFollowRule("#^(?=.*".$last_path.")(?=".$mat.")$# i");
			}
		} else $crawler->addURLFollowRule("#(".$last_path.")# i");
}*/

if (isset($typ) && isset($mat) && !empty($mat)) {
	if ($typ == 'contains') {
		$crawler->addURLFollowRule("#(\b".$mat."\b)# i");
	}
	if ($typ == 'ends') {
		$crawler->addURLFollowRule("#(".$mat.")$# i");
	}
}
//contains rule $crawler->addURLFollowRule("#^http://php.net/manual/en/.*mysql[^a-z]# i");
//ends in rule $crawler->addURLFollowRule("#(htm|html)$# i");

$crawler->go();

// At the end, after the process is finished, we print a short
//$report (see method getProcessReport() for more information)
/*$report = $crawler->getProcessReport();

$lb = "\n";
    
$page = "Summary:".$lb;
$page .= "Links followed: ".$report->links_followed.$lb;
$page .= "Documents received: ".$report->files_received.$lb;
$page .= "Bytes received: ".$report->bytes_received." bytes".$lb;
$page .= "Process runtime: ".$report->process_runtime." sec".$lb;*/

echo WpScraperProCrawler::$url_list;
wp_die();
}

function wp_scraper_pro_help_page() {
	echo '<style type="text/css">
	.fusion-one-half img {
		max-width: 100%;
		box-shadow: 3px 3px 5px 5px #ccc;
	}
	.fusion-layout-column {
		float: left;
		margin-right: 4%;
		position: relative;
	}
	.fusion-one-half {
		width: 46%;
	}
	.fusion-sep-clear {
		clear: both;
		display: block;
		font-size: 0;
		height: 1px;
		line-height: 0;
		overflow: hidden;
		width: 90%;
	}
	.fusion-separator {
		clear: both;
		position: relative;
		z-index: 11;
		border-top: #e0dede solid 1px;
		margin-bottom: 30px;
		margin-left: auto;
		margin-right: auto;
	}
	</style>
	<h1>Documentation</h1>
				<div class="fusion-one-half fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">
<h3>
<strong>Installation</strong>
</h3>
<h4>Uploading via WordPress Dashboard</h4>
<ol>
<li>Navigate to the ‘Add New’ in the plugins dashboard</li>
<li>Navigate to the ‘Upload’ area</li>
<li>Select wp-scraper-pro.zip from your computer</li>
<li>Click ‘Install Now’</li>
<li>Activate the plugin in the Plugin dashboard</li>
</ol>
<h4>Using FTP</h4>
<ol>
<li>Download wp-scraper-pro.zip</li>
<li>Extract the wp-scraper-pro.zip directory to your computer</li>
<li>
Upload the wp-scraper-pro directory to the
<code>/wp-content/plugins/</code>
directory
</li>
<li>Activate the plugin in the Plugin dashboard</li>
</ol>

</div></div><div class="fusion-one-half fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><p>

<img class="alignnone size-full wp-image-126" src="'.plugins_url( "images/Install-Plugin.jpg", __FILE__ ).'" alt="Install Plugin" />

</p>
</div></div><div class="fusion-clearfix"></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;"></div>

<div class="fusion-one-half fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">

<h3>
<strong>Generating URL’s</strong>
</h3>

<p><strong>URL Visual Selector</strong></p>
<p>The visual selector works much in the same way as the Content Selector. The difference is you will only need to select one link to the pages you wish to scrape into your site. The visual selector will then import all similar links into a list for you to use with the multiple scraper.</p>

<p><strong>PHP Crawler</strong></p>

<p>
<strong>Domain Pattern:</strong>
</p>
<p>Only follow links with the same url. – www.example.com and sub.example.com</p>
<p>Only follow links with the same domain. – www.example.com not sub.example.com</p>
<p>
Only follow links in the same path as the given url. – If the url is
<br>
www.example.com/path/index.html, only get urls in www.example.com/path/
</p>
<p>
<strong>Number of Pages:</strong><br>
Allowed Options: 10, 25, 50, 75 and 100 – This sets the amount of webpages to pull from the url.</p>
<p>
<strong>Skip Links:</strong>
<br>
Optionally skip a certain number of links. This is useful if you have already scraped a number of links from a website and want to scrape more pages now. For example, if you already created posts with 10 links from this url, and now you want to grab the next 10 links, you would enter 10 into the box above.
</p>
<p>
<strong>Depth Limit:</strong>
<br>
Optionally set the depth limit for crawling pages. If this value is set to 1, it will only gather webpages that are linked on the entry page. If it is set to 2, it will also gather all webpages linked to the pages found on the entry page.
</p>
<p>
<strong>Request Delay: Seconds</strong>
<br>
Optionally delay each request to the url. This can keep your site from making too many requests at once to the url.
</p>
<p>
<strong>Path Matching: Contains – Ends with</strong>
<br>
Optionally add a word to match within urls.
<br>
For example, choosing “contains foo” above would only add webpages to the list that have “foo” in the path such as example.com/foo or example.com/path/this-page-has-foo
</p>
<p>Note: The list of URL’s will vary in quantity and accuracy depending on the site your retrieving them from.</p>
<div class="fusion-clearfix"></div>

</div></div><div class="fusion-one-half fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><p>

<img class="alignnone size-full wp-image-110" src="'.plugins_url( "images/URL-Selection-1200x1066.jpg", __FILE__ ).'" alt="Generating URL\'s" /><br><br>
<img class="alignnone size-full wp-image-110" src="'.plugins_url( "images/URL-List.jpg", __FILE__ ).'" alt="Generating URL\'s" />

</p>
</div></div><div class="fusion-clearfix"></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;"></div>

<div class="fusion-one-half fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">

<h3><strong>Content Selection</strong></h3>

<p><strong>Highlighting And Selection</strong><br>
You may select as much content as you wish by simply clicking to select the blocks of content you want. As your mouse hovers over the page, a blue box will appear to illustrate what content you will get. If there is an area within the blue box that you do not wish to include, simply click it again. A red box will appear inside the blue box to illustrate content that will be excluded.</p>

<p><strong>Add selected content</strong><br>
Hit the add selected content to my post button on top of window and the content will be added to the WP Scraper post editor.</p>

<p><strong>How much should I select</strong><br>
Depending on server resources adding content to your post may take anywhere from a few seconds to a few minutes. The more content you import into your post, the longer scraping will take. If it takes too long to scrape, you could try increasing the “Time Delay Between Scrapes” under Extract Options.</p>

<div class="fusion-clearfix"></div>

</div></div><div class="fusion-one-half fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><p>

<img class="alignnone size-full wp-image-110" src="'.plugins_url( "images/Saturrn-1200x923.jpg", __FILE__ ).'" alt="Content Selection - Single Scrape" />

</p>
</div></div><div class="fusion-clearfix"></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;"></div>
<div class="fusion-one-half fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">

<h3>
<strong>Single Scrape </strong>
</h3>
<p>
<strong>*URL</strong>
<br>
Enter the URL you wish to copy content from.
</p>
<p>
<strong>*Title</strong>
<br>
You may select a title from the source page or add your own.
</p>
<p>
<strong>*Post Content</strong>
<br>
You may select multiple areas of the source page including images.
</p>
<p>
<strong>Post Type</strong>
<br>
Post Type: Post, Page – Status: Published, Draft, Pending Review
</p>
<p>
<strong>Options</strong>
<br>
Only Text and Images – Checked will remove all html elements except p, div, table, list, break, headings, span, and images. Links are automatically removed with this option. All ids and classes are also removed.
<br><br>
Remove Links – Checked will remove all external links from the content.
<br><br>
Add source link to the content – Checked will Add source link to the content.
</p>
<p>
<strong>Categories</strong>
<br>
Select a category or create a new one.
</p>
<p>
<strong>Tags</strong>
<br>
Select tags from source page or add your own.
</p>
<p>
<strong>Featured Image</strong>
<br>
Select an image from the source page or add your own.
</p>
<p>* Required</p>
<div class="fusion-clearfix"></div>

</div></div><div class="fusion-one-half fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><p>

<img class="alignnone size-full wp-image-205" src="'.plugins_url( "images/Add-New-Scraped-Post-1200x1066.jpg", __FILE__ ).'" alt="Single Scrape" />

</p>
</div></div><div class="fusion-clearfix"></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;"></div>

<div class="fusion-one-half fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">

<h3>
<strong>Multiple Scrape </strong>
</h3>
<p>
<strong>*Titles</strong>
<br>
*Select a title from source page or add your own.
</p>
<p>
<strong>*Post Content</strong>
<br>
*You may select multiple areas of the source page including images.
</p>
<p>
<strong>Post Type</strong>
<br>
Post Type: Post, Page – Status: Published Draft, Pending Draft
</p>
<p>
<strong>Options</strong>
<br>
Include Images, Format Tables, Remove Links, Add source link to the content</p>

<p>
<strong>HTML Options</strong>
<br>Strip all HTML, Include Post HTML, Include Basic HTML, or you can specify exactly which HTML to keep in the content
</p>
<p>
<strong>*Categories</strong>
<br>
Select a category or create a new one.
</p>
<p>
<strong>*Tags</strong>
<br>
*Select tags from source page or add your own.
</p>
<p>
<strong>*Featured Image</strong>
<br>
Select an image from the source page or add your own.
</p>

<p><strong>Extract Options</strong></p>

<p><strong>Load JavaScript</strong><br>
Some content may need javascript enabled to display correctly. Check this box to enable javascript while selecting content.</p>

<p><strong>Load Restricted Image Content</strong><br>
Some images will not load due to cross domain conflicts. Use this feature to load these restricted images. However, it doesn’t work with all server configurations. Use with caution.</p>

<p><strong>Time Delay Between Scrapes</strong><br>
This option will scrape one page at a time with this delay between each post. This will help manage your server resources. Choices Include: None, Ten Seconds, Thirty Seconds or One Minute</p>

<p>*If you type in your own content into the multiple scraper fields then the content will be repeated throughout all the posts. If you choose the content from the source page for any of these fields then the scraper will find and add the content to each post.</p>
<div class="fusion-clearfix"></div>

</div></div><div class="fusion-one-half fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><p>

<img class="alignnone size-full wp-image-133" src="'.plugins_url( "images/Add-mulitple-Scraped-Post-1-1200x1009.jpg", __FILE__ ).'" alt="Multiple Scrape"  /><br><br>
<img class="alignnone size-full wp-image-133" src="'.plugins_url( "images/Add-mulitple-Scraped-Post-e1509110908161.jpg", __FILE__ ).'" alt="Multiple Scrape"  />

</p>
</div></div><div class="fusion-clearfix"></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;"></div><div class="fusion-one-half fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">

<h3>
<strong>WP Scraper Results</strong>
</h3>
<p>The results of the scraper will be shown in real time as posts are created. Please remain on this page until all of your posts are created or it will interrupt the scraping process. Each post will display as soon as it is made. You can view or edit any of the new posts from this screen by simply clicking the provided links. When scraping is complete the progress bar will be removed and a message will be displayed showing that the process is now complete. After completion you are free to navigate from the page.</p>


<p><strong>Errors When Scraping</strong></p>

<p>There are usually only two reasons that a page fails to scrape. The first is if your php allowed memory size is too small to handle the scrape. You can change your php.ini settings to allow for a higher memory_limit. The other main reason scrapes fail is from the selector not being exactly the same on all pages. If this is the case simply rescrape the remaining pages with new selectors.</p>

<p>If you consistently receive multiple errors, try increasing the “Time Delay Between Scrapes” under Extract Options.</p>

<p>WP Scraper Pro will supply you with a list of url’s that failed so you can try again.</p>

<div class="fusion-clearfix"></div>

</div></div><div class="fusion-one-half fusion-layout-column fusion-column-last fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper"><p>

<img class="alignnone size-full wp-image-205" src="'.plugins_url( "images/Results-1-e1509128031325-1200x675.jpg", __FILE__ ).'" alt="Scraper Results" /><br><br>
<img class="alignnone size-full wp-image-205" src="'.plugins_url( "images/errors-text.jpg", __FILE__ ).'" alt="Scraper Results" />

</p>
</div></div><div class="fusion-clearfix"></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-single" style="border-color:#e0dede;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:px;margin-bottom:30px;"></div>
<div class="fusion-full fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;"><div class="fusion-column-wrapper">

<p>WP Scraper Pro is intended solely for copying content that is in the public domain or other wise not protected by any copyright laws in any country.</p>
<p>Please obey the copyright laws of the country you are copying content from. Wp Scraper Pro does not assume any sort of legal responsibility or liability for the consequences of copying content that is protected under any copyright law of any country.</p>
<p>
For more information about copyright laws please visit
<a href="http://www.copyright.gov/">http://www.copyright.gov/</a>
.
</p>
<div class="fusion-clearfix"></div>
</div>
</div>
<div class="fusion-clearfix"></div>

<a href="http://www.wpscraper.com/" target="_blank">For more information please visit us at wpscraper.com</a>
';


}

function wpsp_body_button() {
	return '<a id="choose_body_content" title="Click to select content you want to use for the post content. Then click the button below to add it to the post content field." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button block-select-btn">Choose Post Content</a>';
}

function wpsp_post_tags_meta_box( $post, $box ) {
	$defaults = array( 'taxonomy' => 'post_tag' );
	if ( ! isset( $box['args'] ) || ! is_array( $box['args'] ) ) {
		$args = array();
	} else {
		$args = $box['args'];
	}
	$r = wp_parse_args( $args, $defaults );
	$tax_name = esc_attr( $r['taxonomy'] );
	$taxonomy = get_taxonomy( $r['taxonomy'] );
	$user_can_assign_terms = current_user_can( $taxonomy->cap->assign_terms );
	$comma = _x( ',', 'tag delimiter' );
?>
<div class="tagsdiv" id="<?php echo $tax_name; ?>">
	<div class="jaxtag">
	<div class="nojs-tags hide-if-js">
	<p><?php echo $taxonomy->labels->add_or_remove_items; ?></p>
	<textarea name="<?php echo "tax_input-$tax_name"; ?>" rows="3" cols="20" class="the-tags" id="tax-input-
	<?php echo $tax_name; ?>" 
	<?php disabled( ! $user_can_assign_terms ); ?>>
	<?php if (isset($post->ID)) { echo str_replace( ',', $comma . ' ', get_terms_to_edit( $post->ID, $tax_name ) );} else echo str_replace( ',', $comma . ' ', get_terms_to_edit( '', $tax_name ) ); // textarea_escaped by esc_attr() ?></textarea></div>
 	<?php if ( $user_can_assign_terms ) : ?>
	<div class="ajaxtag hide-if-no-js">
		<label class="screen-reader-text" for="new-tag-<?php echo $tax_name; ?>"><?php echo $box['title']; ?></label>
        <input class="wpsp-selector" type="text" name="tags_selector" value="" id="tags_selector" />
		<p><input type="text" id="new-tag-<?php echo $tax_name; ?>" name="newtag-<?php echo $tax_name; ?>" class="newtag form-input-tip" size="16" autocomplete="off" value="" />
		<input type="button" class="button tagadd" value="<?php esc_attr_e('Add'); ?>" /></p>
		<a id="choose_tags_content" title="Click to select content you want to use for the tags. Then click the button below to add it to the tags field. Remember to use a field that has comma separated values." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button block-select-btn">Choose Tags</a>
	</div>
	<p class="howto"><?php echo $taxonomy->labels->separate_items_with_commas; ?></p>
	<?php endif; ?>
	</div>
	<div class="tagchecklist"></div>
</div>
<?php if ( $user_can_assign_terms ) : ?>
<p class="hide-if-no-js"><a href="#titlediv" class="tagcloud-link" id="link-<?php echo $tax_name; ?>"><?php echo $taxonomy->labels->choose_from_most_used; ?></a></p>
<?php endif; ?>
<?php
}

function wpsp_post_categories_meta_box( $post, $box ) {
	$defaults = array( 'taxonomy' => 'category' );
	if ( ! isset( $box['args'] ) || ! is_array( $box['args'] ) ) {
		$args = array();
	} else {
		$args = $box['args'];
	}
	$r = wp_parse_args( $args, $defaults );
	$tax_name = esc_attr( $r['taxonomy'] );
	$taxonomy = get_taxonomy( $r['taxonomy'] );
	?>
	<div id="taxonomy-<?php echo $tax_name; ?>" class="categorydiv">
    	<input class="wpsp-selector" type="text" name="cat_selector" value="" id="cat_selector" />
		<ul id="<?php echo $tax_name; ?>-tabs" class="category-tabs">
			<li class="tabs"><a href="#<?php echo $tax_name; ?>-all"><?php echo $taxonomy->labels->all_items; ?></a></li>
			<li class="hide-if-no-js"><a href="#<?php echo $tax_name; ?>-pop"><?php _e( 'Most Used' ); ?></a></li>
		</ul>

		<div id="<?php echo $tax_name; ?>-pop" class="tabs-panel" style="display: none;">
			<ul id="<?php echo $tax_name; ?>checklist-pop" class="categorychecklist form-no-clear" >
				<?php $popular_ids = wp_popular_terms_checklist( $tax_name ); ?>
			</ul>
		</div>

		<div id="<?php echo $tax_name; ?>-all" class="tabs-panel">
			<?php
            $name = ( $tax_name == 'category' ) ? 'post_category' : 'tax_input[' . $tax_name . ']';
            echo "<input id='post_category' type='hidden' name='{$name}[]' value='0' />"; // Allows for an empty term set to be sent. 0 is an invalid Term ID and will be ignored by empty() checks.
            ?>
			<ul id="<?php echo $tax_name; ?>checklist" data-wp-lists="list:<?php echo $tax_name; ?>" class="categorychecklist form-no-clear">
				<?php if (isset($post->ID)) { wp_terms_checklist( $post->ID, array( 'taxonomy' => $tax_name, 'popular_cats' => $popular_ids ) );} else wp_terms_checklist( '', array( 'taxonomy' => $tax_name, 'popular_cats' => $popular_ids ) );  ?>
			</ul>
		</div>
	<?php if ( current_user_can( $taxonomy->cap->edit_terms ) ) : ?>
			<div id="<?php echo $tax_name; ?>-adder" class="wp-hidden-children">
				<h4>
					<a id="<?php echo $tax_name; ?>-add-toggle" href="#<?php echo $tax_name; ?>-add" class="hide-if-no-js">
						<?php
							/* translators: %s: add new taxonomy label */
							printf( __( '+ %s' ), $taxonomy->labels->add_new_item );
						?>
					</a>
				</h4>
				<p id="<?php echo $tax_name; ?>-add" class="category-add wp-hidden-child">
					<label class="screen-reader-text" for="new<?php echo $tax_name; ?>"><?php echo $taxonomy->labels->add_new_item; ?></label>
					<input type="text" name="new<?php echo $tax_name; ?>" id="new<?php echo $tax_name; ?>" class="form-required form-input-tip" value="<?php echo esc_attr( $taxonomy->labels->new_item_name ); ?>" aria-required="true"/>
					<label class="screen-reader-text" for="new<?php echo $tax_name; ?>_parent">
						<?php echo $taxonomy->labels->parent_item_colon; ?>
					</label>
					<?php wp_dropdown_categories( array( 'taxonomy' => $tax_name, 'hide_empty' => 0, 'name' => 'new' . $tax_name . '_parent', 'orderby' => 'name', 'hierarchical' => 1, 'show_option_none' => '&mdash; ' . $taxonomy->labels->parent_item . ' &mdash;' ) ); ?>
					<input type="button" id="<?php echo $tax_name; ?>-add-submit" data-wp-lists="add:<?php echo $tax_name; ?>checklist:<?php echo $tax_name; ?>-add" class="button category-add-submit" value="<?php echo esc_attr( $taxonomy->labels->add_new_item ); ?>" />
					
					<?php wp_nonce_field( 'add-' . $tax_name, '_ajax_nonce-add-' . $tax_name, false ); ?>
					<span id="<?php echo $tax_name; ?>-ajax-response"></span>
				</p>
			</div>
		<?php endif; ?>
        <a id="choose_cat_content" title="Click to select content you want to use for the category. Then click the button below to add it to the categories field." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button block-select-btn">Choose a New Category</a>
	</div>
	<?php
}

function wpsp_post_thumbnail_meta_box( $post ) {
	echo '<img class="wpsp_featured" src="" style="display:none" />
		<input class="wpsp-selector" type="text" name="fi_selector" value="" id="fi_selector" />
		<input id="wpsp_featured_image" type="hidden" name="featured_image" value="" />
		<p class="hide-if-no-js"> 
		<a id="set-featured-thumbnail" class="setfeatured" href="#" title="Set featured image">Set featured image</a>
		</p>
		<a id="choose_image_content" title="Click to select the image you want to use for the featured image. Then click the button below to add it to the featured image field." href="#TB_inline?width=600&height=550&inlineId=content-extractor" class="thickbox button block-select-btn">Choose a New Featured Image</a>';
	//echo _wp_post_thumbnail_html( $thumbnail_id, $post->ID );
} 

		
add_action( 'wp_ajax_wpsp_custom_fields', 'wp_scraper_pro_custom_fields');
function wp_scraper_pro_custom_fields() {
	if (isset($_REQUEST['post_type'])) {
		$post_type = $_REQUEST['post_type'];
	} else die(json_encode(array('message' => 'ERROR', 'code' => 2000)));
	
	$obj = get_post_type_object( $post_type );
	$title = post_type_supports($post_type, 'title');
	$editor = post_type_supports($post_type, 'editor');
	$thumbnail = post_type_supports($post_type, 'thumbnail');
	
	if ($post_type == 'post') { $tags = 1; $cat = 1; }
	else {$tags = 0; $cat = 0;}
	echo $title.', '.$editor.', '.$thumbnail.', '.$tags.', '.$cat;
	//print('<pre>'.print_r($obj,true).'</pre>');
	die();
}
?>