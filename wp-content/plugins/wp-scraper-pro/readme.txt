=== WP Scraper ===
Contributors: Allyson Rico, Robert Macchi
Tags: wp-scraper, scraper, content copier, content, copy, website copier. web scraper,
Requires at least: 
Tested up to: 4.6
Stable Tag: trunk 

This plugin allows you to copy content from websites directly to your WordPress posts.

== Description ==

=Installation=

Uploading via WordPress Dashboard

    Navigate to the ‘Add New’ in the plugins dashboard
    Navigate to the ‘Upload’ area
    Select wp-scraper-pro.zip from your computer
    Click ‘Install Now’
    Activate the plugin in the Plugin dashboard

Using FTP

    Download wp-scraper-pro.zip
    Extract the wp-scraper-pro.zip directory to your computer
    Upload the wp-scraper-pro directory to the /wp-content/plugins/ directory
    Activate the plugin in the Plugin dashboard

= Single Scrape =

*URL
Enter the URL you wish to copy content from.

*Title
You may select a title from the source page or add your own.

*Post Content
You may select multiple areas of the source page including images.

Post Type
Post Type: Post, Page – Status: Published, Draft, Pending Review

Options
Remove Links – Checked will remove all external links from the content.
Include CSS – Checked will Include CSS from the scraped page.
Remove Video Tags – Checked will remove all video tags from the content.
Add source link to the content – Checked will Add source link to the content.

Categories
Select a category or create a new one.

Tags
Select tags from source page or add your own.

Featured Image
Select an image from the source page or add your own.

* Required

= Content Selection – Single Scrape =

Highlighting And Selection
You may select as much content as you wish by simply highlighting and selecting the blocks of content you want.

Add selected content
Hit the add selected content to my post button on top of window and the content will be added to the WP Scraper Pro post editor. Edit and post as you like.

How much should I select
Depending on server resources adding content to your post may take anywhere from a few seconds to a few minutes.

= Generating URL’s =

Domain Pattern:

Only follow links with the same url. – www.example.com and sub.example.com

Only follow links with the same domain. – www.example.com not sub.example.com

Only follow links in the same path as the given url. – If the url is
www.example.com/path/index.html, only get urls in www.example.com/path/

Number of Pages:
Allowed Options: 10, 25, 50, 75 and 100 – This sets the amount of webpages to pull from the url.

Skip Links:
Optionally skip a certain number of links. This is useful if you have already scraped a number of links from a website and want to scrape more pages now. For example, if you already created posts with 10 links from this url, and now you want to grab the next 10 links, you would enter 10 into the box above.

Depth Limit:
Optionally set the depth limit for crawling pages. If this value is set to 1, it will only gather webpages that are linked on the entry page. If it is set to 2, it will also gather all webpages linked to the pages found on the entry page.

Request Delay: Seconds
Optionally delay each request to the url. This can keep your site from making too many requests at once to the url.

Path Matching: Contains – Ends with
Optionally add a word to match within urls.
For example, choosing “contains foo” above would only add webpages to the list that have “foo” in the path such as example.com/foo or example.com/path/this-page-has-foo

Note: The list of URL’s will vary in quantity and accuracy depending on the site your retrieving them from.

= Multiple Scrape =

*Titles
*Select a title from source page or add your own.

*Post Content
*You may select multiple areas of the source page including images.

Post Type
Post Type: Post, Page – Status: Published Draft, Pending Draft

Options
Remove Links – Checked will remove all external links from the content.
Include CSS – Checked will Include CSS from the scraped page.
Video Tags – Checked will remove all video tags from the content.
Add source link to the content – Checked will Add source link to the content.

Categories
Select a category or create a new one.

Tags
*Select tags from source page or add your own.

Featured Image
*Select an image from the source page or add your own.

*If you type in your own content into the multiple scraper fields then the content will be repeated throughout all the posts.

*If you choose the content from the source page for any of these fields then the scraper will find and add the content to each post.

*Required

= Content Selection – Multiple Scrape =

Similar to the single scrape but with the ability to choose the content from the source page  allowing the scraper to find and add the content to each post.

Highlighting And Selection
You may select as much content as you wish by simply highlighting and selecting the blocks of content you want.

Add selected content
Hit the add selected content to my post button on top of window and the content will be added to the WP Scraper post editor.

How much should I select
Depending on server resources adding content to your post may take anywhere from a few seconds to a few minutes.

= WP Scraper Results =

The results of the scraper will be shown in real time as posts are created. Please remain on this page until all of your posts are created or it will interrupt the scraping process. Each post will display as soon as it is made. You can view or edit any of the new posts from this screen by simply clicking the provided links. When scraping is complete the progress bar will be removed and a message will be displayed showing that the process is now complete. After completion you are free to navigate from the page.

===========================================

WP Scraper Pro is intended solely for copying content that is in the public domain or other wise not protected by any copyright laws in any country.

Please obey the copyright laws of the country you are copying content from. Wp Scraper Pro does not assume any sort of legal responsibility or liability for the consequences of coping content that is protected under any copyright law of any country.

For more information about copyright laws please visit http://www.copyright.gov/.

== ChangeLog ==

= Version 1.0 =

* Initial version of this plugin.