(function($) {

jQuery(document).ready(function($) {
	$('#tryagain').click(function(e) {
			e.preventDefault();
			$('#retry').submit();
		});
		var index = 0;
		var form_data = $('#wpsp-add-multi-post-form input, #wpsp-add-multi-post-form textarea, #wpsp-add-multi-post-form select').serialize();
		var error_list = '';
		var wpspinterval = $('input[name=wpspinterval]:checked').val();
		var urls = $('#wpsp-url-list').val();
		var newurls = $.trim(urls);
		if (newurls.match(",\n$")) {
			newurls = newurls.slice(0,-3)
		}
		if (newurls.match(",$")) {
			newurls = newurls.slice(0,-1)
		}
		var url_array = newurls.split(',\n');
		url_array.shift();
		var totalUrls = url_array.length;
		var completeUrls = 0;
		console.log(totalUrls);
		$.ajax({
			type: "POST",
            url: $('#wpsp-add-multi-post-form').attr('action'),
            data: form_data,
			complete: function(data) {
				var title = $('#title_prefix').val()+$('#title').val()+$('#title_suffix').val();
				var s = data.responseText;
				s = s.substring(s.indexOf('{'));
				var response = $.parseJSON(s);
				var html = $('#wpsp-scraper-results tbody').html();
				$('#wpsp-scraper-results tbody').html(html+'<tr class="iedit author-self level-0 type-post format-standard hentry"><td class="title column-title has-row-actions column-primary page-title" data-colname="Title">'+title+'</td><td class="view column-view has-row-actions column-primary page-view"><a style="padding-left: 5px;" target="_blank" href="'+response.view+'">View</a></td> <td class="edit column-edit has-row-actions column-primary page-edit"><a style="padding-left: 5px;" target="_blank" class="post-edit-link" href="'+response.edit+'">Edit</a></td></tr>');
				$('#wpsp-scraper-results').show();
				var totalUrls = url_array.length;
				var completeUrls = 0;
				
				if (totalUrls != 0) {
				$.each(url_array, function(key, id) {
					setTimeout(function() {
					$.ajax({
						type: "POST",
						url: ajax_object.ajax_url,
						data: form_data + '&ThisUrl=' + id + '&action=wpsp_multi_scrape_action',
						dataType: "json",
						success: function(response) {
						completeUrls = completeUrls + 1;
						var entry = response;
						console.log(entry);
						if (entry == "ERROR" || entry == null || entry.title == 'undefined' || entry == 0) {
							var html = $('#wpsp-scraper-results tbody').html();
							error_list += id+',\r';
							$('#wpsp-scraper-results tbody').html(html+'<tr class="iedit author-self level-0 type-post format-standard hentry"><td colspan="3" class="title column-title has-row-actions column-primary page-title" data-colname="Title">There was an error scraping the url <a href="'+id+'" target="_blank">'+id+'</a>. Please try again.</td></tr>');
						} else {
						var html = $('#wpsp-scraper-results tbody').html();
						$('#wpsp-scraper-results tbody').html(html+'<tr class="iedit author-self level-0 type-post format-standard hentry"><td class="title column-title has-row-actions column-primary page-title" data-colname="Title">'+entry.title+'</td><td class="view column-view has-row-actions column-primary page-view"><a style="padding-left: 5px;" target="_blank" href="'+entry.view+'">View</a></td> <td class="edit column-edit has-row-actions column-primary page-edit"><a style="padding-left: 5px;" target="_blank" class="post-edit-link" href="'+entry.edit+'">Edit</a></td></tr>');
						}
						if (completeUrls >= totalUrls) {
							$('.wpsp-form').hide('slow');
							$('.wpsp-form').removeClass('loading');
							if ($('#message').length == 0 ) {
								$('#wpsp-scraper-results').prepend('<div id="message" class="updated notice is-dismissible"><p>Scraping Complete!</p></div>');
								if (error_list != '') {
								$('#url_list').val(error_list);
								$('#retry').show();
								}
							}
						}
						},
						error: function(jqXHR, response) {
						console.log(jqXHR);
						console.log(response);
						error_list += id+',\r';
						completeUrls = completeUrls + 1;
						var html = $('#wpsp-scraper-results tbody').html();
						$('#wpsp-scraper-results tbody').html(html+'<tr class="iedit author-self level-0 type-post format-standard hentry"><td colspan="3" class="title column-title has-row-actions column-primary page-title" data-colname="Title">There was an error scraping the url <a href="'+id+'" target="_blank">'+id+'</a>. Please try again.</td></tr>');
						if (completeUrls >= totalUrls) {
							$('.wpsp-form').hide('slow');
							$('.wpsp-form').removeClass('loading');
							if ($('#message').length == 0 ) {
								$('#wpsp-scraper-results').prepend('<div id="message" class="updated notice is-dismissible"><p>Scraping Complete!</p></div>');
								if (error_list != '') {
								$('#url_list').val(error_list);
								$('#retry').show();
								}
							}
						}
						},
						});
						}, index*wpspinterval);
						index++;
					});
				
				}
			},
            dataType: 'json'
        });
		
});
		
			
	

})(jQuery);