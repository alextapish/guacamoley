(function($) {

jQuery(document).ready(function($) {
	
	$('input:radio[name="htmlelems"]').change(
    function(){
        if ($(this).is(':checked') && $(this).val() == 'specific') {
            $('textarea#wpsp-elements').removeAttr('disabled');
        } else {
            $('textarea#wpsp-elements').attr("disabled", "disabled");
        }
    });
	
	$('.hndle, .handlediv').click(function() {
        $(this).parent().toggleClass('closed');
		if ($(this).parent().hasClass('closed')) {
			$('.wpsp-settings .handlediv .dashicons').removeClass('dashicons-arrow-up');
			$('.wpsp-settings .handlediv .dashicons').addClass('dashicons-arrow-down');
		} else {
			$('.wpsp-settings .handlediv .dashicons').removeClass('dashicons-arrow-down');
			$('.wpsp-settings .handlediv .dashicons').addClass('dashicons-arrow-up');
		}
    });
	
	$('#wpsp-crawl-submit').click(function(e) {
		e.preventDefault();
		$('.wpsp-php-urls').addClass('loading');
		$('.hndle, .handlediv').parent().removeClass('closed');
		var url = $('#wpsp-url').val();
		var fol = $('input[name=wpsp_follow]:checked').val();
		var num = $('input[name=wpsp_number]:checked').val();
		var error = false;
		if(url == '') {
			$('<p class="wpsp-error-notice">Url is a required field.</p>').insertAfter('#wpsp-urld');
			error = true;
		}
		if(!fol) {
			$('<p class="wpsp-error-notice">Domain Pattern is a required field.</p>').insertAfter('#wpsp-pattern');
			error = true;
		}
		if(!num) {
			$('<p class="wpsp-error-notice">Number of Pages is a required field.</p>').insertAfter('#wpsp-num');
			error = true;
		}
		if (error == false) {
		$.ajax({
			type: "POST",
			url: ajax_object.ajax_url,
			data: {
				"action": "wpsp_ajax_scrape",
				"url":url,
				"fol":fol,
				"num":num,
				"skp":$('#wpsp-skip').val(),
				"dep":$('#wpsp-depth').val(),
				"del":$('#wpsp-delay').val(),
				"typ":$('#wpsp-typematch option:selected').val(),
				"mat":$('input[name=wpsp_pattern]').val(),
			},
			timeout: 600000,
			success: function(data) {
				$('#wpsp-url-list').val(data);
				$('.hndle, .handlediv').parent().addClass('closed');
				$('.wpsp-php-urls').removeClass('loading');
				//location.reload(true);
				//window.location = self.location;
            },
			error: function() {
				$('#wpsp-url-list').val('Something went wrong. Most likely, your server timeout limits have been reached. Set the number of pages to a smaller number and try again.');
				$('.hndle, .handlediv').parent().addClass('closed');
				$('.wpsp-php-urls').removeClass('loading');
			}
		})
		} else { $('.wpsp-php-urls').removeClass('loading'); }
	});
	
	$('#wpsp-continue-submit').click(function(e) {
		var error = false;
		if($('#wpsp-url-list').val() == '') {
			$('<p class="wpsp-error-notice">Webpages to Scrape is a required field.</p>').insertAfter('#wpsp-continue-submit');
			error = true;
		}
		if (error == true) {
			e.preventDefault();
		}
		if (error == false) {
			return true;
		}
	});
	
	
	$('input[name=fix]').change(function(){
        if ($(this).is(':checked')) {
			$('input[name=title]').addClass('fixes');
			$('input[name=title_prefix]').show();
			$('input[name=title_suffix]').show();
		} else {
			$('input[name=title]').removeClass('fixes');
			$('input[name=title_prefix]').hide();
			$('input[name=title_suffix]').hide();
		}
	});
	
	$('.admin_page_wp-scraper-pro-add-menu #toplevel_page_wp-scraper-pro').removeClass('wp-not-current-submenu');
	$('.admin_page_wp-scraper-pro-add-menu #toplevel_page_wp-scraper-pro').addClass('wp-has-current-submenu');
	$('.admin_page_wp-scraper-pro-add-menu #toplevel_page_wp-scraper-pro a.wp-not-current-submenu').addClass('wp-has-current-submenu');
	$('.admin_page_wp-scraper-pro-add-menu #toplevel_page_wp-scraper-pro a.wp-not-current-submenu').removeClass('wp-not-current-submenu');
	$('.admin_page_wp-scraper-pro-add-menu #toplevel_page_wp-scraper-pro ul li:nth-child(3n)').addClass('current');

	$('.wpsp-selector').focus(function() {
		if ($(this).attr('id') != 'live_selector') {
			$('<p class="wpsp-notice description">Changing this field affects how data is pulled from your list of urls. Please only change this value if you understand how to use CSS selectors to choose the data for each field. Thank you!</p>').insertAfter(this).delay(10000).fadeOut();
		}
		if ($(this).attr('id') == 'live_selector') {
			$('<p class="wpsp-notice description">Changing this field affects how data is pulled from the url. Please only change this value if you understand how to use CSS selectors to choose the data for each field. Thank you!</p>').insertAfter(this).delay(10000).fadeOut();
		}
	});

	$('.save-post-status').click(function(e) {
		e.preventDefault();
		var status = $( "#post_status" ).val();
		$('#hidden_post_status').val(status);
		if (status == 'draft') var display = 'Draft';
		if (status == 'publish') var display = 'Published';
		if (status == 'pending') var display = 'Pending Review';
		$('#post-status-display').html(display);
	});
	
	$('.edit-post-type').click(function(e) {
		e.preventDefault();
		$('#post-type-select').show();
	});
	
	$('.cancel-post-type').click(function(e) {
		e.preventDefault();
		$('#post-type-select').hide();
	});
	
	$('.save-post-type').click(function(e) {
		e.preventDefault();
		var type = $( "#post_type" ).val();
		$('#hidden_post_type').val(type);
		var display = type.substr(0,1).toUpperCase()+type.substr(1);
		$('#post-type-display').html(display);
		$('#post-type-select').hide();
		
		jQuery.post(ajax_object.ajax_url,{
				"action": "wpsp_custom_fields",
				"post_type": type,
			},function(response){
				$metaArr = response.split(', ');
				if ($metaArr[0] == 0) {
					$('#titlediv').hide();
				} else $('#titlediv').show();
				if ($metaArr[1] == 0) {
					$('#wpsp-data').hide();
				} else $('#wpsp-data').show();
				if ($metaArr[2] == 0) {
					$('#postimagediv').hide();
				} else $('#postimagediv').show();
				if ($metaArr[3] == 0) {
					$('#tagsdiv-post_tag').hide();
				} else $('#tagsdiv-post_tag').show();
				if ($metaArr[4] == 0) {
					$('#categorydiv').hide();
				} else $('#categorydiv').show();
			}
		);
	});
	
	var custom_uploader1;
 
    $('#set-featured-thumbnail').click(function(e) {
        e.preventDefault();
		var set = false;
 		if($(this).hasClass('remove')) {
			$('.wpsp_featured').attr('src', '');
			$('.wpsp_featured').hide();
            $('#wpsp_featured_image').val('');
			$('#set-featured-thumbnail').removeClass('remove');
			$('#set-featured-thumbnail').html('Set Featured Image');
			$('#choose_image_content').show();
			set = true;
		}
		if(set == false) {
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader1) {
            custom_uploader1.open();
            return;
        }
 
        //Extend the wp.media object
        custom_uploader1 = wp.media.frames.file_frame = wp.media({
            title: 'Featured Image',
            button: {
                text: 'Set Featured Image'
            },
            multiple: false
        });
 
        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader1.on('select', function() {
            attachment = custom_uploader1.state().get('selection').first().toJSON();
			$('.wpsp_featured').attr('src', attachment.url);
			$('.wpsp_featured').show();
            $('#wpsp_featured_image').val(attachment.id);
			$('#set-featured-thumbnail').addClass('remove');
			$('#set-featured-thumbnail').html('Remove Featured Image');
			$('#choose_image_content').hide();
        });
 
        //Open the uploader dialog
        custom_uploader1.open();
		}
 
    });
	
	$('#choose_body_content, #choose_title_content, #choose_tags_content, #choose_cat_content, #choose_image_content').click(function () {
	var url = $('#wpsp-url').val().trim();
	if (!url) {
		alert('URL not set');
		return false;
	} else {
		var refrsh = false;
		if($(this).attr('id') == 'choose_body_content') {
			if ($('#body_selector').val() != '') { refrsh = true;}
			$('#content-extractor-iframe').addClass('wpsbody');
			$('#content-extractor-iframe').removeClass('wpstitle');
			$('#content-extractor-iframe').removeClass('wpstags');
			$('#content-extractor-iframe').removeClass('wpscat');
			$('#content-extractor-iframe').removeClass('wpsimage');
			$('#wpsp-html-html').click();
		}
		if($(this).attr('id') == 'choose_title_content') {
			if ($('#title_selector').val() != '') { refrsh = true;}
			$('#content-extractor-iframe').addClass('wpstitle');
			$('#content-extractor-iframe').removeClass('wpsbody');
			$('#content-extractor-iframe').removeClass('wpstags');
			$('#content-extractor-iframe').removeClass('wpscat');
			$('#content-extractor-iframe').removeClass('wpsimage');
		}
		if($(this).attr('id') == 'choose_tags_content') {
			if ($('#tags_selector').val() != '') { refrsh = true;}
			$('#content-extractor-iframe').addClass('wpstags');
			$('#content-extractor-iframe').removeClass('wpsbody');
			$('#content-extractor-iframe').removeClass('wpstitle');
			$('#content-extractor-iframe').removeClass('wpscat');
			$('#content-extractor-iframe').removeClass('wpsimage');
		}
		if($(this).attr('id') == 'choose_cat_content') {
			if ($('#cat_selector').val() != '') { refrsh = true;}
			$('#content-extractor-iframe').addClass('wpscat');
			$('#content-extractor-iframe').removeClass('wpsbody');
			$('#content-extractor-iframe').removeClass('wpstitle');
			$('#content-extractor-iframe').removeClass('wpstags');
			$('#content-extractor-iframe').removeClass('wpsimage');
		}
		if($(this).attr('id') == 'choose_image_content') {
			if ($('#fi_selector').val() != '') { refrsh = true;}
			$('#content-extractor-iframe').addClass('wpsimage');
			$('#content-extractor-iframe').removeClass('wpsbody');
			$('#content-extractor-iframe').removeClass('wpstitle');
			$('#content-extractor-iframe').removeClass('wpstags');
			$('#content-extractor-iframe').removeClass('wpscat');
		}
		$('#TB_window').css({ 'bottom': '30px', 'left': '30px', 'position': 'fixed', 'right': '30px', 'top': '30px', 'z-index': '160000' });
		if($("input[name=down]").prop('checked') == true){
			var down = 1;
		} else {
			var down = 0;
		}
		if($("input[name=js]").prop('checked') == true){
			var js = '&js=true';
		} else {
			var js = '';
		}
		
		var newSrc = $('#wpsp-content-extractor-url').val()+'&blockUrl='+encodeURIComponent($('#wpsp-url').val())+'&down='+down+js;
		if($('#content-extractor-iframe').attr('src') != newSrc || refrsh == true ) {
			$('#content-extractor-iframe').attr('src', newSrc);
		}
		$('#TB_overlay').fadeIn();
		$('#TB_window').fadeIn();
	}
	});
	
$('#choose_links').click(function() {
	
	var url = $('#wpsp-link-url').val().trim();
	if (!url) {
		alert('URL not set');
		return false;
	} else {
	$('#content-extractor-iframe').addClass('wpslinks');
	
	$('#TB_window').css({ 'bottom': '30px', 'left': '30px', 'position': 'fixed', 'right': '30px', 'top': '30px', 'z-index': '160000' });
	
	var newSrc = $('#wpsp-content-extractor-url').val()+'&blockUrl='+encodeURIComponent($('#wpsp-link-url').val())+'&down=0&js=true&url=1';
	if($('#content-extractor-iframe').attr('src') != newSrc) {
		$('#content-extractor-iframe').attr('src', newSrc);
		
	}
	$('#TB_overlay').fadeIn();
	$('#TB_window').fadeIn();
	}
});



$('#multi_submit').click(function(e) {
	e.preventDefault();
	$('.wpsp-form').addClass('loading');
		var error = false;
		jQuery('#wpsp-html-html').click();
		if($('#title').val() == '') {
			$('<p class="wpsp-error-notice">Title is a required field.</p>').insertAfter('#title');
			error = true;
		}
		if($('#wpsp-url').val() == '') {
			$('<p class="wpsp-error-notice">Page Url is a required field.</p>').insertAfter('#wpsp-url');
			error = true;
		}
		if($('#wpsp-html').val() == '') {
			$('<p class="wpsp-error-notice">Page Content is a required field.</p>').insertAfter('#wpsp-html');
			error = true;
		}
		if (error == false) {
        	$('#wpsp-add-multi-post-form').submit();
		} else { 
        $('.wpsp-form').removeClass('loading');
		}
        return false;
});

$('#auto_submit').click(function(e) {
	e.preventDefault();
	$('.wpsp-form').addClass('loading');
		var error = false;
		jQuery('#wpsp-html-html').click();
		if($('#title').val() == '') {
			$('<p class="wpsp-error-notice">Title is a required field.</p>').insertAfter('#title');
			error = true;
		}
		if($('#wpsp-url').val() == '') {
			$('<p class="wpsp-error-notice">Page Url is a required field.</p>').insertAfter('#wpsp-url');
			error = true;
		}
		if($('#wpsp-html').val() == '') {
			$('<p class="wpsp-error-notice">Page Content is a required field.</p>').insertAfter('#wpsp-html');
			error = true;
		}
		if (error == false) {
			var htmlelems = $('input[name=htmlelems]:checked').val();
			var wpspinterval = $('input[name=wpspinterval]:checked').val();
			var action = $('#wpsp-add-multi-post-form').attr('action');
        	$('#wpsp-add-multi-post-form').attr('action', action+'&htmlelems='+htmlelems+'&wpspinterval='+wpspinterval);
        	$('#wpsp-add-multi-post-form').submit();
		} else { 
        $('.wpsp-form').removeClass('loading');
		}
        return false;
});

$('#wpsp-add-post-form').submit(function() {
	
        $('.wpsp-form').addClass('loading');
		var error = false;
		jQuery('#wpsp-html-html').click();
		if($('#title').val() == '') {
			$('<p class="wpsp-error-notice">Title is a required field.</p>').insertAfter('#title');
			error = true;
		}
		if($('#wpsp-url').val() == '') {
			$('<p class="wpsp-error-notice">Page Url is a required field.</p>').insertAfter('#wpsp-url');
			error = true;
		}
		if($('#wpsp-html').val() == '') {
			$('<p class="wpsp-error-notice">Page Content is a required field.</p>').insertAfter('#wpsp-html');
			error = true;
		}
		if (error == false) {
        tinyMCE.triggerSave();
        $.ajax({
			type: "POST",
            url: $(this).attr('action'),
            data: $('#wpsp-add-post-form input, #wpsp-add-post-form textarea, #wpsp-add-post-form select').serialize(),
			complete: function(data) {
				var s = data.responseText;
				s = s.substring(s.indexOf('{'));
				var response = $.parseJSON(s);
				window.location = response.redirect_url;
            },
            dataType: 'json'
        });
		} else { 
        $('.wpsp-form').removeClass('loading');
		}
        return false;
    }); 

}); 


$('#wpsp-select-html, #wpsp-select-links').click(function() {
	
	window.frames['wpsp-extractor'].resetvars();
	window.frames['wpsp-extractor'].liveHtml();
});

$('#wpsp-select-urls').click(function() {
	window.frames['wpsp-extractor'].urlList();
});

})(jQuery);

function generateUrls(urls) {
	jQuery('#wpsp-url-list').html(urls);
	jQuery('.overlay-loading').hide();
	jQuery('body.modal-open').css('overflow', 'scroll');
	jQuery('#TB_overlay').fadeOut();
	jQuery('#TB_window').fadeOut();
	jQuery('#wpsp-visual-generate .handlediv').click();
};

function liveData(this_path, sec_path, html, images, not_this_path)
{
	var url = jQuery('#wpsp-url').val();
	var multiple = false;
	/*if(jQuery('#wpsp_is_mult').val() == 'true') {
		multiple = true;
		jQuery('.overlay-loading').show();
	}*/
	
	if (not_this_path != '') {
		not_this_path = " :not("+not_this_path+")";
	}
	
    if(jQuery('#content-extractor-iframe').hasClass('wpsbody')) {
		jQuery('#wpsp-images').val(images);
		jQuery('#wpsp-data').show();
			jQuery('textarea#wpsp-html').val(html);
			jQuery('#content-extractor-iframe').removeClass('wpsbody');
			jQuery('body.modal-open').css('overflow', 'scroll');
				
			jQuery('#wpsp-html-tmce').click();
			
			if (jQuery('#body_selector').length != 0) {
			jQuery('#body_selector').val(this_path+not_this_path);
			jQuery('#body_selector').show();
			}
			
		
		
	} else if(jQuery('#content-extractor-iframe').hasClass('wpstitle')) {
			var title = jQuery(html).text();
			jQuery('#title').val(title);
			if (jQuery('#title_selector').length != 0) {
			jQuery('#title_selector').val(this_path+not_this_path);
			jQuery('#title_selector').show();
			}	
		jQuery('#content-extractor-iframe').removeClass('wpstitle');
		jQuery('body.modal-open').css('overflow', 'scroll');
	} else if (jQuery('#content-extractor-iframe').hasClass('wpstags')) {
			if (jQuery('#tags_selector').length != 0) {
				jQuery('#tags_selector').val(this_path+not_this_path);
				jQuery('#tags_selector').show();
			}
			var tags = jQuery(html).text();
			jQuery('#new-tag-post_tag').val(tags);
			jQuery('.tagadd').click();
		
		jQuery('#content-extractor-iframe').removeClass('wpstags');
		jQuery('body.modal-open').css('overflow', 'scroll');
	} else if(jQuery('#content-extractor-iframe').hasClass('wpscat')) {
		
			if (jQuery('#cat_selector').length != 0) {
				jQuery('#cat_selector').val(this_path+not_this_path);
				jQuery('#cat_selector').show();
			}
			var cat = jQuery(html).text();
			jQuery('#newcategory').val(cat);
			jQuery('#category-add-submit').click();
		
		jQuery('#content-extractor-iframe').removeClass('wpscat');
		jQuery('body.modal-open').css('overflow', 'scroll');
	} else if(jQuery('#content-extractor-iframe').hasClass('wpsimage')) {
		if(images == '') {
			var new_html = jQuery.parseHTML(html);
			var new_image = jQuery(new_html).find('img').attr('src');
			images = new_image;
		}
		if (jQuery('#fi_selector').length != 0) {
			jQuery('#fi_selector').val(this_path+not_this_path);
			jQuery('#fi_selector').show();
		}
			jQuery('.wpsp_featured').attr('src', images);
			jQuery('.wpsp_featured').show();
			jQuery('#wpsp_featured_image').val(images);
			jQuery('#set-featured-thumbnail').addClass('remove');
			jQuery('#set-featured-thumbnail').html('Remove Featured Image');
			jQuery('#choose_image_content').hide();
			jQuery('#content-extractor-iframe').removeClass('wpsimage');
						jQuery('.overlay-loading').hide();
		
		jQuery('body.modal-open').css('overflow', 'scroll');
	} 
	jQuery('#TB_overlay').fadeOut();
	jQuery('#TB_window').fadeOut();
	//jQuery('#content-extractor-iframe').removeAttr('src').empty();
    
}