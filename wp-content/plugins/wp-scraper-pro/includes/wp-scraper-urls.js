(function($) {

jQuery(document).ready(function($) {
	var this_path = '';
	var url_list = '';
	var first_url = '';
	var auto_next = '';
	
	/*jQuery.fn.extend({
		getPath: function () {
			var path, node = this;
			if (node[0].id) return "#" + node[0].id;
			while (node.length) {
				var realNode = node[0], name = realNode.localName;
				if (!name) break;
				name = name.toLowerCase();
	
				var parent = node.parent();
	
				var sameTagSiblings = parent.children(name);
				if (sameTagSiblings.length > 1) { 
					allSiblings = parent.children();
					var index = allSiblings.index(realNode) + 1;
					if (index > 1) {
						name += ':nth-child(' + index + ')';
					}
				}
	
				path = name + (path ? '>' + path : '');
				node = parent;
			}
	
			return path;
		/*if (this.length != 1) throw 'Requires one element.';
		var path, node = this;
		if (node[0].id) return "#" + node[0].id;
		while (node.length) {
			var realNode = node[0],
				name = realNode.localName;
			if (!name) break;
			name = name.toLowerCase();
			var parent = node.parent();
			var siblings = parent.children(name);
			if (siblings.length > 1) {
				name += ':eq(' + siblings.index(realNode) + ')';
			}
			path = name + (path ? '>' + path : '');
			node = parent;
		}
		return path;
		}
	});*/
	
    $('a').click(function() {
        return false;
    });

    $('form').submit(function() {
        return false;
    });

	 $('a').mousemove(function() {
        if ($(':hover', $(this)).length) {
			$(this).removeClass('wpscraper-hover');	
        } else {
			if(!$(this).parent().closest('.wpscraper-selected').length) {
				$(this).addClass('wpscraper-hover');	
			}			
        }
    }).mouseout(function() {
			$(this).removeClass('wpscraper-hover');	
    }).click(function() {
			
        if (!$(':hover', $(this)).length) {
			if($(this).find('.wpscraper-selected')) {
					$(this).find('.wpscraper-selected').each(function(index, element) {
                        $(this).removeClass('wpscraper-selected');
                    });
			}
			if(!$(this).parent().closest('.wpscraper-selected').length) {
				if(!$(this).hasClass('wpscraper-selected')) {
            		$('*').removeClass('wpscraper-selected');
					$(this).addClass('wpscraper-selected');
				} else {
					$(this).removeClass('wpscraper-selected');
				}
			}
        }
    });
	
	window.resetvars = function() {
		this_path = '';
		html_code = '';
		pg_imgs = [];
		images = '';
	}
	
	window.theSelector = function(thisObj) {
		var closestID = thisObj.closest('[id]');
		var closestClass = thisObj.closest('[class]');
		var idNum = closestID.offset();
		var classNum = closestClass.offset();
		while (closestClass.attr('class') == '') {
			closestClass.removeAttr('class');
			closestClass = closestClass.closest('[class]');
		}
		if (typeof idNum === 'undefined' && typeof classNum === 'undefined') {
			var thisParent = thisObj.parent();
			var parTag = thisParent.prop('tagName').toLowerCase();
			var likeEls = $(parTag);
			var parTotalIndex = likeEls.index( thisParent );
			var parStr = '';
			while (parTotalIndex != 0 && tagName != 'tbody') {
				var parTag = thisParent.prop('tagName').toLowerCase();
				var likeEls = $(parTag);
				var parTotalIndex = likeEls.index( thisParent );
				var thisPar = thisParent.parent();
				likeEls = thisPar.find(thisParent);
				var parIndex = likeEls.index(thisParent);
				parStr = ' > '+parTag+parStr;
				thisParent = thisParent.parent();
			}
			var thisTag = thisObj.prop('tagName').toLowerCase();
			var thisPar = thisObj.parent();
			likeEls = thisPar.find(thisTag);
			var newIndex = likeEls.index(thisObj);
			returnStr = returnStr+parStr+' > '+thisTag;
			return returnStr;
		} else if (typeof idNum === 'undefined') {
			if (closestClass.is(thisObj)) {
				var thisClasses = thisObj.attr('class');
				thisClasses = thisClasses.split(" ");
				thisClasses = thisClasses.pop();
				var likeEls = $('.'+thisClasses);
				index = likeEls.index( closestClass );
				return '.'+thisClasses;
			} else {
				var topClasses = closestClass.attr('class');
				var classes = topClasses.split(" ");
				classes = classes.pop();
				var likeEls = $('.'+classes);
				index = likeEls.index( closestClass );
				var returnStr = '.'+classes;
				
				var thisParent = thisObj.parent();
				var parStr = '';
				while (!thisParent.is(closestClass)) {
					var parTag = thisParent.prop('tagName').toLowerCase();
					var thisPar = thisParent.parent();
					likeEls = thisPar.find(parTag);
					var parIndex = likeEls.index(thisParent);
					parStr = ' > '+parTag+parStr;
					thisParent = thisParent.parent();
				}
				var thisTag = thisObj.prop('tagName').toLowerCase();
				var thisPar = thisObj.parent();
				likeEls = thisPar.find(thisTag);
				var newIndex = likeEls.index(thisObj);
				returnStr = returnStr+parStr+' > '+thisTag;
				return returnStr;
			}
		} else if (typeof classNum === 'undefined') {
			if (closestID.is(thisObj)) {
				var thisID = thisObj.attr('id');
				var likeEls = $("#"+thisID);
				index = likeEls.index( closestID );
				return '#'+thisID;
			} else {
				var id = closestID.attr('id');
				var likeEls = $("#"+id);
				index = likeEls.index( closestID );
				var returnStr = '#'+id;
				
				var thisParent = thisObj.parent();
				var parStr = '';
				while (!thisParent.is(closestID)) {
					var parTag = thisParent.prop('tagName').toLowerCase();
					var thisPar = thisParent.parent();
					likeEls = thisPar.find(parTag);
					var parIndex = likeEls.index(thisParent);
					parStr = ' > '+parTag+parStr;
					thisParent = thisParent.parent();
				}
				var thisTag = thisObj.prop('tagName').toLowerCase();
				var thisPar = thisObj.parent();
				likeEls = thisPar.find(thisTag);
				var newIndex = likeEls.index(thisObj);
				returnStr = returnStr+parStr+' > '+thisTag;
				return returnStr;
				
			}
		} else {
			if (closestID.is(thisObj)) {
				var thisID = thisObj.attr('id');
				var likeEls = $("#"+thisID);
				index = likeEls.index( closestID );
				return '#'+thisID;
			} else if (closestClass.is(thisObj)) {
				var thisClasses = thisObj.attr('class');
				thisClasses = thisClasses.split(" ");
				thisClasses = thisClasses.pop();
				var likeEls = $('.'+thisClasses);
				index = likeEls.index( closestClass );
				return '.'+thisClasses;
			}
			if (idNum.top >= classNum.top) {
				var id = closestID.attr('id');
				var likeEls = $("#"+id);
				index = likeEls.index( closestID );
				var returnStr = '#'+id;
				var thisParent = thisObj.parent();
				var parStr = '';
				while (!thisParent.is(closestID)) {
					var parTag = thisParent.prop('tagName').toLowerCase();
					var thisPar = thisParent.parent();
					likeEls = thisPar.find(parTag);
					var parIndex = likeEls.index(thisParent);
					parStr = ' > '+parTag+parStr;
					thisParent = thisParent.parent();
				}
				var thisTag = thisObj.prop('tagName').toLowerCase();
				var thisPar = thisObj.parent();
				likeEls = thisPar.find(thisTag);
				var newIndex = likeEls.index(thisObj);
				returnStr = returnStr+parStr+' > '+thisTag;
				return returnStr;
				
			} else if (classNum.top > idNum.top) {
				var topClasses = closestClass.attr('class');
				var classes = topClasses.split(" ");
				var classes = classes.pop();
				var likeEls = $('.'+classes);
				index = likeEls.index( closestClass );
				var returnStr = '.'+classes;
				var thisParent = thisObj.parent();
				var parStr = '';
				while (!thisParent.is(closestClass)) {
					var parTag = thisParent.prop('tagName').toLowerCase();
					var thisPar = thisParent.parent();
					likeEls = thisPar.find(parTag);
					var parIndex = likeEls.index(thisParent);
					parStr = ' > '+parTag+parStr;
					thisParent = thisParent.parent();
				}
				var thisTag = thisObj.prop('tagName').toLowerCase();
				var thisPar = thisObj.parent();
				likeEls = thisPar.find(thisTag);
				var newIndex = likeEls.index(thisObj);
				returnStr = returnStr+parStr+' > '+thisTag;
				return returnStr;
				
			} else {return 'Error';}
		}
	}
	
	window.urlList = function() {
		if(!$('.wpscraper-selected').length) alert('Please select a link to generate urls.');
		var urls = '';
		$('.wpscraper-selected').each(function (index) {
		
		if ($(this).prop('tagName').toLowerCase() == 'img') {
			if($(this).parent().hasClass('wpscraper-selected-parent')) {
				$(this).unwrap();	
			}
			if($(this).parent().hasClass('wpscraper-hover-parent')) {
				$(this).unwrap();	
			}
		}
		var nthis = this;
		if (!$(':hover', $(nthis)).length) {
		$(nthis).removeClass('wpscraper-selected').removeClass('wpscraper-hover');
		$('*[class=""]').removeAttr('class');
		var elem = theSelector( $(this) );
		elem = elem.replace(/\:eq\(\d+\)/g, '');
		$(elem).each( function(index, url) {
			var href = $(url).attr('href');
			if (urls == '') {
				urls = href;
			} else {
				urls += ',\n'+href;
			}
		});
		}
		
		
		
		});
		
		$('*').removeClass('wpscraper-hover');
		$('*').removeClass('wpscraper-selected');
		$('.wpscraper-hover-parent').contents().unwrap();
		$('.wpscraper-selected-parent').contents().unwrap();
		$('*').removeClass('wpscraper-not-hover');
		$('*').removeClass('wpscraper-not-selected');
		$('.wpscraper-not-hover-parent').contents().unwrap();
		$('.wpscraper-not-selected-parent').contents().unwrap();
		$('.wpscraper-wrapper').contents().unwrap();
		window.parent.generateUrls(urls);
	}
	
}); 

})(jQuery);