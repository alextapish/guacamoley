<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;
// use Roots\Sage\MobileDetect;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage')
  ]);

  register_nav_menus([
    'secondary_navigation' => __('Secondary Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  add_image_size( 'feature-lg', 780, 410, true );

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  // add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
  wp_enqueue_script('mobiledetect/js', Assets\asset_path('scripts/detectmobilebrowser.js'), ['jquery'], null, true);

  if( is_singular( 'post' ) ) {
    global $wp_query;
    $query = array( 
      'post__not_in' => array( get_queried_object_id() ), 
      // 'category_name' => ea_first_term( 'category', 'slug' ),
      'posts_per_page' => 1 
    );
    $args = array(
      'url'   => admin_url( 'admin-ajax.php' ),
      'query' => $query,
    );
    wp_localize_script( 'sage/js', 'beloadmore', $args );
    // print_r( $args );
  }

}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

/**
 * AJAX Load More Articles
 *
 */
function be_ajax_load_more() {
  // error_reporting(E_ALL); // Debug
  // ini_set("display_errors", 1); // Debug
  $args = isset( $_POST['query'] ) ? $_POST['query'] : array();
  $args['post_type'] = isset( $args['post_type'] ) ? esc_attr( $args['post_type'] ) : 'post';
  $args['paged'] = esc_attr( $_POST['page'] );
  $args['post_status'] = 'publish';
  ob_start();
  $loop = new \WP_Query( $args );
  if( $loop->have_posts() ): while( $loop->have_posts() ): $loop->the_post();
    get_template_part('views/partials/content-single', get_post_type());
  endwhile; endif; wp_reset_postdata();
  $data = ob_get_clean();
  wp_send_json_success( $data );
  wp_die();
}
add_action( 'wp_ajax_be_ajax_load_more', __NAMESPACE__ . '\\be_ajax_load_more' );
add_action( 'wp_ajax_nopriv_be_ajax_load_more', __NAMESPACE__ . '\\be_ajax_load_more' );
/**
 * First Term 
 * Helper Function
 */
function ea_first_term( $taxonomy, $field ) {
  $terms = get_the_terms( get_the_ID(), $taxonomy );
  
  if( empty( $terms ) || is_wp_error( $terms ) )
    return false;
  
  // If there's only one term, use that
  if( 1 == count( $terms ) ) {
    $term = array_shift( $terms );
  } else {
    $term = array_shift( $list );
  }
  
  // Output   
  if( $field && isset( $term->$field ) )
    return $term->$field;
  
  else
    return $term; 
  
}

/**
 * Add ACF theme options support
 */
if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page();
  
}