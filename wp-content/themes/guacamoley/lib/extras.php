<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Get the_featured_image()
 */
function the_featured_image($size = 'feature-lg') {
  echo '<figure class="featured-image">';
  if(!is_single()) { echo '<a href="'.get_the_permalink().'">'; }
    the_post_thumbnail($size);
  if(!is_single()) { echo '</a>'; }
  if (get_post( get_post_thumbnail_id() )->post_excerpt) {
    echo '<figcaption class="caption">' . get_post( get_post_thumbnail_id() )->post_excerpt . '</figcaption>';
  }
  echo '</figure>';
}

/**
 * Get first_cat_name()
 */
function first_cat_name() {
  $category = get_the_category();
  $currentcat = $category[0]->cat_ID;
  $currentcatname = $category[0]->cat_name;
  $currentcatslug = $category[0]->slug;
  echo '<a href="/'. $currentcatslug .'" class="first-category">'. $currentcatname .'</a>';
}

/**
 * Display time as "ago"
 */
function time_ago() {
  return human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago' );
}