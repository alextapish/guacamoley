<div class="container posts-container">

	<div class="row">

		<div class="column col-md-12 col-lg-8 offset-lg-1">

		<?php if (!have_posts()) : ?>
		  <div class="alert alert-warning">
		    <?php _e('Sorry, no results were found.', 'sage'); ?>
		  </div>
		  <?php get_search_form(); ?>
		<?php endif; ?>

		<?php while (have_posts()) : the_post(); ?>
		  <?php get_template_part('views/partials/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
		<?php endwhile; ?>

		</div>

		<div class="sidebar-home hidden-md-down">
			<?php get_template_part('views/partials/sidebar'); ?>
		</div>

	</div>

	<div class="row pagenav-container">
		<?php the_posts_pagination( array(
		    'prev_text' => __( 'Newer Articles', 'textdomain' ),
		    'next_text' => __( 'Older Articles', 'textdomain' ),
		) ); ?>
	</div>



</div>