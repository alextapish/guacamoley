<?php
	while (have_posts()) : the_post();
		get_template_part('views/partials/content-single', get_post_type());
	endwhile;
?>
