export default {
  init() {
    // JavaScript to be fired on all pages
    $(".hamburger").click(function () {
      $(this).toggleClass("is-active");
      $('.main-header').toggleClass("expanded");
      $('.overlay').toggleClass("is-active");
    });

    $(".overlay").click(function () {
      $(this).toggleClass("is-active");
      $('.main-header').toggleClass("expanded");
      $('.hamburger').toggleClass("is-active");
    });

    $(".share-btn").click(function (e) {
      e.preventDefault();
      $(".share-modal").toggleClass("is-active");
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
