export default {
  init() {
    // JavaScript to be fired on the home page

    if($.browser.mobile == false){
      // eslint-disable-next-line no-undef, no-unused-vars
      var stickyAd_home = new Waypoint.Sticky({
        element: $('.sidebar-home')[0],
        wrapper: false,
      });
    }

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
