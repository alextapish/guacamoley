export default {
	init() {
	// JavaScript to be fired on the single posts

		$( window ).on( "load", function() {

			var page = 2;
			
			var bindWaypoints = function($items, $ads) {
				// eslint-disable-next-line no-undef, no-unused-vars
				var inviewAd = new Waypoint({
					element: $ads,
					handler: function(direction) {
						var previousAd = $('.article-container:not(:last-of-type)').find('.sidebar-article');
						if (direction === "down") {
							$(previousAd).removeAttr("style");
							$(this.element).css({
								"position": "fixed",
								"top": "100px",
								"right": "0px",
							});
						} else if (direction === "up") {
							$(this.element).removeAttr("style");
						}
					},
					offset: '50%',
				});

				// eslint-disable-next-line no-undef, no-unused-vars
				var inviewArticle = new Waypoint({
					element: $items,
					handler: function(direction) {
						if (direction === "down") {
							addmore();
							this.disable();
						}
					},
					offset: 'bottom-in-view',
				});
			};

			bindWaypoints($('.load-more'), $('.sidebar-article'));

			var addmore = function () {
				$.ajax({
					// eslint-disable-next-line no-undef
					url: beloadmore.url,
					type: 'post',
					data: {
						action: 'be_ajax_load_more',
						// eslint-disable-next-line no-undef
						query: beloadmore.query,
						page: page,
					},
					beforeSend: function() {
						$('.load-more').remove();
						$('.pagination-single').append( '<span class="loader">Loading...</span>' );
					},
					success: function( res ) {
						$('#main').append( res.data );
						// eslint-disable-next-line no-undef
						ga('send', 'pageview', $('.article-container:last-of-type').attr("data-url"));
						page = page + 1;
						// loading = false;
						$('.loader').remove();
						history.pushState({}, '', $('.article-container:last-of-type').attr("data-url"));
						// console.log($('.article-container:last-of-type').attr("data-url")+' enter');
						// eslint-disable-next-line no-undef
						deployads.newPage();
					},
					complete: function(){
						var $newarticle = $('.article-container:last-of-type').find('.load-more');
						var $newads = $('.article-container:last-of-type').find('.sidebar-article');
						bindWaypoints($newarticle, $newads);
						// eslint-disable-next-line no-undef
						Waypoint.refreshAll();
					},
				})
			};

		});

		// Refresh Waypoints whenever the Spot.IM iframe resizes
		// eslint-disable-next-line no-unused-vars
		document.addEventListener('spot-im-frame-resize', function(event) {
			// event.detail includes information relevant to this event
			// eslint-disable-next-line no-undef
			Waypoint.refreshAll();
		});

	},
};
