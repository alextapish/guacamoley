<?php
/**
 * Template Name: Flexible
 */
?>

<?php 
$args = array( 'headers' => array( 
    'Authorization' => 'Basic ZjEyOjIwMTc=', 
    'authtoken' => 'eyJhbGciOiJIUzI1NiJ9.MTQ5NzM3ODMzNDQ1Mg.wYAf2zvv9lQsq8MrkPLdKoWd-azESV3RxE1Tye2XV30',
    'organizationid' => '3398e496-5d1b-4999-b962-2d0a7fb86443',
    'applicationid' => 'e5e9a5e1-13be-479d-8ef5-3c698059aa97',
    'Content-Type' => 'application/x-www-form-urlencoded',
    'Postman-Token' => '238f7c61-38f5-490e-93ba-159a6f3cdb5e',
	'Cache-Control' => 'no-cache'
  ) );

// foreach(range(0, 106) as $i) {
  $url = WP_CONTENT_URL.'/json/guac-import-3.json';

  $request = wp_remote_get( $url, $args );

  if( is_wp_error( $request ) ) {
  	echo $request->get_error_message();
  	return false; // Bail early
  }

  $body = wp_remote_retrieve_body( $request );

  $data = json_decode( $body );

  if( ! empty( $data ) ) {

  	// open the file "demosaved.csv" for writing
  	$filepath = ABSPATH.'wp-content/uploads/guacpermalinks1.csv';
  	$file = fopen($filepath, 'w');
  	 
  	// send the column headers
  	// fputcsv($file, array('old_permalink', 'new_permalink'));
    $user = array();
  	foreach( $data->pkg->documents as $article ) {
      // $sid = $article->sid.'/';
      // $old_permalink = trim($article->permalink, "-");
      // $new_permalink = str_replace($sid,"",$old_permalink);

      // echo "<p>".$old_permalink." | ".$new_permalink."</p>";

      $user[] = $article->user->firstName;
      $user[] = $article->user->lastName;
      $user[] = $article->user->email;
      $user[] = $article->user->bio;
      $user[] = $article->user->twitter;
      $user[] = $article->user->image->originalFileUrl;

      // $categories=array();
      // foreach ($obj->photos as $photo){
      //     $categories[]=$photo->category;
      // }
      

      // $unique_users = array_unique($user);
      // print_r($unique_users);
      // foreach($unique_users as $user) {
      //   echo '<p>'.$user['first_name'].' '.$user['last_name'].' '.$user['email'].'</p>';
      // }

      // echo '<p>'.$user_first_name.' '.$user_last_name.' '.$user_email.'</p>';

  		// $title = wp_encode_emoji($article->title);
  		// $title_slug = trim($article->slug, "-");
  		// $excerpt = $article->excerpt;

  		// foreach( $article->categories as $category ) {
    //     $cat .= $category->slug.', ';
    //   }

    //   $featured_image = $article->featuredImage->originalFileUrl;

    //   if ($article->featuredImage->attribution == true) {
    //   	$featured_image_caption = $article->featuredImage->attributionName;
    //   }
    //   elseif ($article->featuredImage->attribution == true && !empty($article->featuredImage->attributionUrl)) {
    //   	$featured_image_caption = '<a href="'.$article->featuredImage->attributionUrl.'" class="featured-attribution">'.$article->featuredImage->attributionName.'</a>';
    //   }

    //   $publish_date = $article->publishedAt;
    //   $author_email = $article->user->email;

    //   foreach( $article->fields as $field ) {
    //   	if ($field->type == 'text') {
    //   		$content .= htmlspecialchars_decode($field->props->value);
    //   	}
    //     	elseif ($field->type == 'embed') {
    // 			if ($field->props->digitalAsset->mimetype == 'twitter/tweet' || $field->props->digitalAsset->mimetype == 'youtube/mp4') {
    //     			$content .= htmlspecialchars_decode($field->props->digitalAsset->html);
    //     		}
    //     		else {
    //     			$content .= '<img src="'.$field->props->digitalAsset->originalFileUrl.'">';
    //     		}
    //     	}
    //   }

//   		$sorted_data = array(
//   			array($old_permalink, $new_permalink),
//   		);// output each row of the data
//       foreach ($sorted_data as $row)
//       {
//       fputcsv($file, $row);
//       // unset($content);
//       // unset($cat);
//       unset($sorted_data);
//       // print_r($row);
//       }
  	}

    $user=array_unique($user);

    print implode('; ', $user);
  	 

  	 
//   	// Close the file
//   	fclose($file);
//     echo "Completed ".$i;
// } // iterative loop close

	// foreach( $data->pkg->documents as $article ) {
 //      echo '<div>';
 //        echo '<h1>'.$article->title.'</h2>';
 //        echo '<p>'.trim($article->slug, "-").'</p>';
 //        echo '<p>'.trim($article->excerpt, "-").'</p>';
 //        echo "<p>";
 //        foreach( $article->categories as $category ) {
 //          echo $category->slug.' ';
 //          if ($category->slug == 'lol') {
 //          	$catID[] = 4;
 //          }
 //          if ($category->slug == 'covfefe') {
 //          	$catID[] = 3;
 //          }
 //          if ($category->slug == 'extra-chunky') {
 //          	$catID[] = 2;
 //          }
 //          if ($category->slug == 'the-scoop') {
 //          	$catID[] = 1;
 //          }
 //        }
	// 	print_r($catID);
	// 	unset($catID);
 //        echo "</p>";
 //        echo '<img src="'.$article->featuredImage->originalFileUrl.'">';
 //        if ($article->featuredImage->attribution == true) {
        	
 //        }
 //        echo '<p>'.$article->publishedAt.'</p>';
 //        echo '<p>'.$article->user->username.', '.$article->user->email.'</p>';
 //        // echo '<img src="'.$article->user->image->url.'">';
 //        foreach( $article->fields as $field ) {
 //        	if ($field->type == 'text') {
 //        		echo '<div>'.$field->props->value.'</div>';
 //        	}
 //          	elseif ($field->type == 'embed') {
 //      			// echo $field->props->digitalAsset->mimetype;
 //          		if ($field->props->digitalAsset->mimetype == 'twitter/tweet') {
 //          			echo '<div>'.$field->props->digitalAsset->html.'</div>';
 //          		}
 //          		else {
 //          			echo '<img src="'.$field->props->digitalAsset->originalFileUrl.'">';
 //          		}
          		
 //          	}
 //        }
 //      echo '</div>';
 //    }
}
?>

<?php //if ( have_rows( 'content' ) ) : while ( have_rows('content' ) ) : the_row(); ?>
 
<?php //endwhile; endif; ?>
