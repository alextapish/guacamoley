<?php use Roots\Sage\Titles; ?>

<div class="banner-primary py-5 bg-primary">
  <div class="container">
    <h1 class="display-1 icon-white">
      <?php get_template_part( 'dist/images/icons/inline', 'envelope.svg' ); ?>
      Canary
    </h1>
    <p class="lead mr-5 pr-5">This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and
      three supporting pieces of content. Use it as a starting point to create something more unique. </p>
    <p>
      <a class="btn btn-light btn-lg" href="#" role="button"><span><?php get_template_part( 'dist/images/icons/inline', 'phone-volume.svg' ); ?></span>Learn more </a>
    </p>
  </div>
</div>
