<?php use Roots\Sage\Extras; ?>

<div class="container article-container article-infinite" data-url="<?php the_permalink(); ?>">

  <div class="row">

    <div class="column col-md-12 col-lg-8">
      <?php //while (have_posts()) : the_post(); ?>
        <article <?php post_class(); ?>>
          <header>
            <?php if(get_field('featured_video_option')) : ?>
                <div class='embed-container'>
                  <?= the_field('featured_video'); ?>
                </div>
              <?php else :
                Extras\the_featured_image();
              endif;
            ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php get_template_part('views/partials/entry-meta'); ?>
          </header>
          <div class="entry-content">
            <?php the_content(); ?>
          </div>
          <footer>
            <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
          </footer>
          <?php next_post_link( '<button class="next-article btn">%link <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>', 'Next Article' ); ?>
          <?php
            global $withcomments;
            $withcomments = true;
            comments_template('/templates/comments.php');
          ?>
        </article>
      <?php //endwhile; ?>
    </div>

    <div class="hidden-md-down col-lg-4">
      <?php get_template_part('views/partials/sidebar'); ?>
    </div>

  </div>

  <div class="row">
    <div class="col-md-12 col-lg-3 text-center mx-auto pagination-single">
      <?php //next_post_link( '<button class="next-article btn">%link <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>', 'Next Article' ); ?>
      <div class="next_post_link"><?php previous_post_link( '%link','Previous Post' ) ?></div>
      <div class="previous_post_link"><?php next_post_link( '%link','Next Post' ) ?></div>
    </div>
  </div>

</div>

<?php //echo do_shortcode( '[ajax_load_more post_type="post" posts_per_page="0" scroll_container="#main" transition_container_classes="row" button_label="Next Article" images_loaded="true"]' ); ?>