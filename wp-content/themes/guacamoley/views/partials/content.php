<?php use Roots\Sage\Extras; ?>

	<article <?php post_class(); ?>>
		<?php
			if(get_field('featured_video_option')) { echo '<a href="'.get_the_permalink().'" class="video-post">'; }
			Extras\the_featured_image();
			if(get_field('featured_video_option')) { echo '</a>'; }
		?>
		<div class="post-content">
			<header>
				<?php Extras\first_cat_name(); ?>
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php get_template_part('views/partials/entry-meta'); ?>
			</header>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div>
		</div>
	</article>
