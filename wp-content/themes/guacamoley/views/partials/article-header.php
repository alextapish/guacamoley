<?php // Template for article header ?>
<div class="header-buttons">
  <div class="share">
    <button class="share-btn btn"><a href="#">Share <i class="fa fa-share" aria-hidden="true"></i></a></button>
    <div class="share-modal">
      <?php social_warfare(); ?>
    </div>
  </div>
  <?php previous_post_link( '<button class="next-article btn">%link <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>', 'Next Article' ); ?>
</div>