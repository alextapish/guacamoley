<?php
  /**
   * Define Mobile Detect
  */
  use Roots\Sage\MobileDetect;
  $detect = new MobileDetect\Mobile_Detect;
?>
<?php $homeURL = home_url(); ?>
<header class="main-header">
  <div class="container">   
    <div class="row">
      <div class="col-sm-12 col-md-7 col-lg-4">
        <button class="hamburger hamburger--spin" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
        <a class="navbar-brand" href="<?= $homeURL; ?>"><img src="<?php the_field('logo', 'option') ?>" class="logo"></a>
      </div>
      <div class="hidden-md-down col-lg-5">
        <nav class="nav-primary">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
          endif;
          ?>
        </nav>
      </div>
      <div class="hidden-sm-down col-md-5 col-lg-3">
        <?php if (is_home()) { get_template_part('views/partials/social', 'header'); } ?>

        <?php 

          if (is_single()) {
            get_template_part('views/partials/article', 'header');
          }
          elseif (!is_home()) {
            get_template_part('views/partials/social', 'header');
          }

        ?>
      </div>
    </div>
  </div>
  <div class="row header-expand">
    <?php if(wp_is_mobile() || $detect->isTablet()) : ?>
      <div class="search-form">
        <?php get_search_form(); ?>
      </div>
      <?php
        if (is_single() && !$detect->isTablet()) : 
          get_template_part('views/partials/article', 'header');
        endif;
      ?>
      <nav class="nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </nav>
    <?php endif; ?>
    <?php if(!wp_is_mobile()) : ?>
      <div class="search-form">
        <?php get_search_form(); ?>
      </div>
    <?php endif; ?>
    <nav class="nav-secondary">
      <?php
      if (has_nav_menu('secondary_navigation')) :
        wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
    <?php if (is_single()) { get_template_part('views/partials/social', 'header'); } ?>
    <div class="copyright">
      <p><?php the_field('copyright', 'option') ?></p>
    </div>
  </div>
</header>