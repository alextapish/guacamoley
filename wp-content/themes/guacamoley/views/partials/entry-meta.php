<?php use Roots\Sage\Extras; ?>

<p class="byline author vcard"><?= __('By', 'sage'); ?> <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?= get_the_author(); ?></a> • <?php if(!is_single()) echo "Updated"; ?> <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= Extras\time_ago(); ?></time></p>
