<?php // Social Icons Template ?>

<ul class="social-links">
<?php if( have_rows('social_accounts', 'option') ): while ( have_rows('social_accounts', 'option') ) : the_row(); ?>
  <li><a href="<?php the_sub_field('social_url'); ?>" target="_blank" class="social-link"><?php the_sub_field('social_icon'); ?></a></li>
<?php endwhile; endif; ?>
  <li><a href="mailto:<?php the_field('contact_email', 'option'); ?>" class="social-link"><i class="fa fa-envelope"></i></a></li>
</ul>
