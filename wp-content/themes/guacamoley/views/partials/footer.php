<footer class="footer-primary">
	<?php dynamic_sidebar('sidebar-footer'); ?>
</footer>
<?php if(wp_is_mobile()) : ?>
	<div class="overlay"></div>
<?php endif; ?>
<!-- Mailchip Popup -->
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us2.list-manage.com","uuid":"8d820d59175ab5f7d4ea0e2c4","lid":"2e6b4c049b","uniqueMethods":true}) })</script>
<!-- End Mailchip Popup -->