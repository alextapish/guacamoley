<?php use Roots\Sage\Titles; ?>

<div class="page-header container">
	<div class="row">
		<div class="col-sm-12 col-md-10 mx-auto">
			<h1 class="page-title"><?= Titles\title(); ?></h1>
		</div>
	</div>
</div>
