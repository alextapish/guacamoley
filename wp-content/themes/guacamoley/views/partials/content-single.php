<?php use Roots\Sage\Extras; ?>

<div class="container article-container" data-url="<?php echo get_the_permalink(); ?>">

  <div class="row">

    <div class="column col-md-12 col-lg-8">
      
        <article <?php post_class(); ?>>
          <header>
            <?php if(get_field('featured_video_option')) : ?>
                <div class='embed-container'>
                  <?= the_field('featured_video'); ?>
                </div>
              <?php else :
                Extras\the_featured_image();
              endif;
            ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php get_template_part('views/partials/entry-meta'); ?>
          </header>
          <div class="entry-content">
            <?php the_content(); ?>
          </div>
          <footer>
            <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
          </footer>

          <?php
              // Set Spot.im vars
              $article_topics = get_the_category();
              $article_url = get_permalink();
              $post_id = get_the_ID();
            ?>

            <div data-spotim-module="recirculation" data-spot-id="sp_RafSirsU"></div>
            <script async src="https://recirculation.spot.im/spot/sp_RafSirsU"></script>
            <script async src="https://launcher.spot.im/spot/sp_RafSirsU" 
                data-spotim-module="spotim-launcher" 
                data-post-url="<?= $article_url; ?>" 
                data-article-tags="<?php foreach ($article_topics as $topic){ if ($topic === end($article_topics)) { echo $topic->cat_name; } else { echo $topic->cat_name.', '; } } ?>" 
                data-post-id="<?= $post_id; ?>"></script>

          <?php
            // global $withcomments;
            // $withcomments = true;
            // comments_template('/templates/comments.php');
          ?>
        </article>
    </div>

    <div class="sidebar-article hidden-md-down col-lg-4">
      <?php get_template_part('views/partials/sidebar'); ?>
    </div>

  </div>

  <div class="row">
    <div class="col-md-12 col-lg-3 text-center mx-auto pagination-single">
      <?php previous_post_link( '<button class="load-more next-article btn">%link <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>','Next Article' ) ?>
    </div>
  </div>

</div>