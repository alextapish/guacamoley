<?php
	// Set vars
	$author_id = get_the_author_meta('ID');
	$author_name = get_the_author_meta('display_name');
	$author_desc = get_the_author_meta('description');
	$author_email = get_the_author_meta('user_email');
?>

<div class="container posts-container">

	<div class="row author-intro">

		<div class="column col-md-12 col-lg-2 offset-lg-2">
			<?php echo get_avatar( get_the_author_meta( 'ID' ), 200, '/wp-content/uploads/2018/10/favicon-guac.png' ); ?>
		</div>

		<div class="column col-md-12 col-lg-5 offset-lg-1">
			<h2>About <?= $author_name; ?></h2>

			<p><?= $author_desc; ?></p>

			<ul class="social-links">
			<?php if( have_rows('user_social_accounts', 'user_'. $author_id) ): while ( have_rows('user_social_accounts', 'user_'. $author_id) ) : the_row(); ?>
			  <li><a href="<?php the_sub_field('user_social_url'); ?>" target="_blank" class="social-link"><?php the_sub_field('user_social_icon'); ?></a></li>
			<?php endwhile; endif; ?>
			  <li><a href="mailto:<?= $author_email; ?>" class="social-link"><i class="fa fa-envelope"></i></a></li>
			</ul>
		</div>
		
	</div>

	<div class="row author-article-title">
		<div class="column col-md-12 col-lg-8 offset-lg-2">
			<h3>Articles by <?= $author_name; ?></h3>
		</div>
	</div>

	<div class="row">

		<div class="column col-md-12 col-lg-8 offset-lg-2">

		<?php if (!have_posts()) : ?>
		  <div class="alert alert-warning">
		    <?php _e('Sorry, no results were found.', 'sage'); ?>
		  </div>
		  <?php get_search_form(); ?>
		<?php endif; ?>

		<?php while (have_posts()) : the_post(); ?>
		  <?php get_template_part('views/partials/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
		<?php endwhile; ?>

		</div>

		<div class="sidebar-home hidden-md-down">
			<?php get_template_part('views/partials/sidebar'); ?>
		</div>

	</div>

<?php the_posts_pagination( array(
    'prev_text' => __( 'Newer Articles', 'textdomain' ),
    'next_text' => __( 'Older Articles', 'textdomain' ),
) ); ?>

</div>